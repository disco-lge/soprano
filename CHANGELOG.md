# Revision history for soprano

## 0.2.0.1 -- 2022-08-16

* package soprano for guix
* improve segmentation
* provide two ways to access the articles: by headword and by rank

## 0.2.0.0 -- 2021-07-07

Release the first fully working version:

* segmenting articles from the flow of pages
* [encoding](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Encoding)
  articles into XML-TEI
* filtering elements featured on the page in the output (captions, page breaks,
  peritext elements…)

## 0.1.0.0 -- 2020-05-02

* First draft, almost working. Still some issues identified with 3 pages
