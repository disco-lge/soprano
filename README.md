# soprano

Soprano is a tool to process ALTO files delivered by the BnF for project [DISCO-LGE](https://www.collexpersee.eu/projet/disco-lge/). You usually won't need to call it directly when processing the whole of *La Grande Encyclopédie* but it will be used by the script [LGEencode.sh](https://gitlab.huma-num.fr/disco-lge/processinglge) instead.

Still, you can use it to [extract specific information like pages headers or process a given set of pages](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/home).

## Installation

Soprano is written in [haskell](https://www.haskell.org/) and can be installed using [cabal](https://www.haskell.org/cabal). From the root directory of this repository just issue

```sh
cabal new-build
```

to build the package, then

```sh
cabal new-install exe:soprano
```

to install it for your user (make sure `$HOME/.cabal/bin/` is in your `$PATH`). This package includes a test suite which can be run with

```sh
cabal new-test
```

## Command line arguments

Here is a complete list of recognized command-line arguments:

- `--charWidth PIXELS`, short `-c`: the expected [width of a character](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Encoding#detecting-article-beginings) in pixels
- `--keep BLOCKTYPES`, short `-k`: block types to [keep](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Cleaning#keep)
- `--lineHeight PIXELS`, short `-l`: the expected [height of a text line](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Cleaning#lineheight) in pixels
- `--logLevel LOG_LEVEL`, short `-L`: set the log level among : Error,Warning,Info,Debug
- `--noise FILE`, short `-n`: path to a file containing characters to [treat as noise](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Cleaning#noise) and filter out
- `--output DIRECTORY`, short `-o`: directory where to generate files
- `--scoriae CSV_FILE`, short `-s`: path to a file containing the [scoriae to delete](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Cleaning#scoriae)
- `--scoriaThreshold THRESHOLD`, short `-T`: minimum required value for [word confidence](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Cleaning#scoriathreshold) in ALTO input
- `--text`, short `-t`: [output](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Home#output-format) text instead of XML
- `--titlePrefix`, short `-P`: prefix to add to each XML-TEI file's title during the [encoding](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Encoding) - defaults to the empty string
- `--raw` the default [processing step](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Home#processing-steps), leaving the input content as unchanged as possible.
- `--fixed` the second [processing step](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Home#processing-steps) that applies fixes but keep the content as a whole
- `--articles` the final [processing step](https://gitlab.huma-num.fr/disco-lge/soprano/-/wikis/Home#processing-steps) that applies fixes and segments the content into articles
