{-# LANGUAGE NamedFieldPuns #-}
module Config (
      Config(..)
    , Format(..)
    , Input(..)
    , Step(..)
    , getContext
    , get
  ) where

import Control.Applicative ((<|>), optional)
import Data.Char (toLower)
import Data.List (intercalate)
import Data.Monoid ((<>))
import Data.Set as Set (Set, empty, fromList)
import Data.Version (showVersion)
import Context (Context(..))
import Control.Applicative ((<**>))
import Log (LogLevel(..))
import Options.Applicative (
      Parser, ReadM, argument, auto, execParser, flag, flag', fullDesc, header
    , help, helper, info, long, metavar, option, short, str, value
  )
import qualified Paths_soprano as Soprano (version)
import Scoria (fromCSV)
import Text.Printf (printf)
import Text.XML.Light.ALTO.Block.Pre (BlockType(..))

data Input = StdIn | FileInput FilePath
data Format = Txt | XML deriving Show
data Step = Raw | Fixed | Articles deriving (Enum, Eq, Ord, Show)

data Config = Config {
      charWidth :: Int
    , format :: Format
    , input :: Input
    , keep :: Set BlockType
    , lineHeight :: Int
    , logLevel :: LogLevel
    , noise :: Maybe FilePath
    , output :: FilePath
    , scoriae :: Maybe FilePath
    , scoriaThreshold :: Float
    , step :: Step
    , titlePrefix :: Maybe String
  }

fileArg :: ReadM Input
fileArg = FileInput <$> str

stepOption :: Parser Step
stepOption = foldr (<|>) (pure Raw) $ f <$> [Raw .. Articles]
  where
    f s = flag' s $ (long $ toLower <$> show s) <> (help $ doc s)
    doc Raw = "leave the input unchanged (default)"
    doc Fixed = "apply ordering, scoria fixes and block filtering"
    doc Articles = "--fixed + output one file per article"

configP :: Parser Config
configP = Config
  <$> option auto (
            short 'c' <> long "charWidth" <> metavar "PIXELS" <> value 20
          <> help "the expected width of a character in pixels"
        )
  <*> flag XML Txt (
          short 't' <> long "text" <> help "output text instead of XML"
        )
  <*> argument fileArg (
          value StdIn <> metavar "INPUT_ALTO_FILE" <> help "file to process"
        )
  <*> option auto (
          let blockTypes = [Header .. Special] in
            short 'k' <> long "keep" <> metavar "BLOCKTYPES"
          <> value (Set.fromList blockTypes)
          <> help (
                printf "block types to keep (defaults to all, that is %s)"
                  (intercalate "," $ show <$> blockTypes)
              )
        )
  <*> option auto (
            short 'l' <> long "lineHeight" <> metavar "PIXELS" <> value 50
          <> help "the expected height of a text line in pixels"
        )
  <*> option auto (
          let logLevels = [Error .. Debug] in
            short 'L' <> long "logLevel" <> metavar "LOG_LEVEL"
          <> value Warning
          <> help (
                printf "set the log level among : %s"
                  (intercalate "," $ show <$> logLevels)
              )
        )
  <*> option (optional str) (
            short 'n' <> long "noise" <> metavar "FILE" <> value Nothing
          <> help "path to a file containing characters to treat as noise and filter out"
        )
  <*> option str (
            short 'o' <> long "output" <> metavar "OUTPUT_DIR"
          <> help "directory where to generate files"
        )
  <*> option (optional str) (
            short 's' <> long "scoriae" <> metavar "CSV_FILE" <> value Nothing
          <> help "path to a file containing the scoriae to delete (with header)"
        )
  <*> option auto (
            short 'T' <> long "scoriaThreshold" <> metavar "THRESHOLD" <> value 0.5
          <> help "Minimum required value for word confidence in ALTO input"
        )
  <*> stepOption
  <*> option (optional str) (
            short 'P' <> long "titlePrefix" <> metavar "TITLE" <> value Nothing
          <> help "String to prepend to the article's title in the <title/> element of TEI output"
        )

get :: IO Config
get = do
  execParser $
    info
      (configP <**> helper)
      (fullDesc <> header ("Soprano v" ++ showVersion Soprano.version))

getContext :: Config -> IO Context
getContext config = do
  denyList <- maybe Set.empty id <$> (mapM Scoria.fromCSV $ scoriae config)
  noiseSet <- maybe Set.empty Set.fromList <$> (mapM readFile $ noise config)
  return $ Context {
        denyList
      , Context.lineHeight = Config.lineHeight config
      , Context.charWidth = Config.charWidth config
      , noiseSet
      , Context.keep = Config.keep config
      , Context.scoriaThreshold = Config.scoriaThreshold config
      , Context.titlePrefix = Config.titlePrefix config
    }
