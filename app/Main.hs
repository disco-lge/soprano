{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
module Main where

import Config (Config(..), Format(..), Input(..), Step(..), getContext, get)
import Context (Context)
import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Reader (ReaderT, asks, runReaderT)
import Control.Monad.Trans (lift)
import Data.List (intercalate)
import Log (LoggerT, handle, io)
import Pipes ((>->), Consumer, await, runEffect)
import Text.Printf (printf)
import Text.XML.Light (Content)
import Text.XML.Light.TEI (Document(..))
import Step (Output(..), articles, fixed, raw)
import System.Directory (createDirectoryIfMissing)
import System.FilePath ((</>), (<.>), (-<.>), takeFileName)
import System.IO (Handle, IOMode(..), hPutStrLn, stderr, withFile)
import System.Posix (createLink)

type T = LoggerT (ReaderT Config IO)

runOn :: (FilePath -> T [Content]) -> FilePath -> ReaderT Config IO ()
runOn f filePath = do
  directory <- asks output
  outputFormat <- asks format
  liftIO (putStrLn filePath)
  filterLogs (Just formatMessage)
    (f filePath >>= write outputFormat directory)
  where
    outputPath d oF = d </> takeFileName filePath -<.> (extension oF)
    write outputFormat directory contents =
      liftIO $ withFile (outputPath directory outputFormat) WriteMode
        (writeOutput outputFormat contents)
    formatMessage = printf "Could not process file '%s' : %s" filePath

extension :: Format -> String
extension XML = ".xml"
extension Txt = ".txt"

filterLogs :: Maybe (String -> String) -> T () -> ReaderT Config IO ()
filterLogs logFilter m = do
  logger <- asks (io . logLevel)
  handle m
    logger
    (liftIO . hPutStrLn stderr . (maybe id id logFilter :: String -> String))

writeOutput :: Output a => Format -> a -> Handle -> IO ()
writeOutput format contents hFile =
  mapM_ (hPutStrLn hFile) $ to format contents
  where
    to Txt = toTxt
    to XML = toXML

createArticles :: Consumer Document (ReaderT Context T) ()
createArticles = do
  inFolder <- lift . lift $ asks ((</>) . output)
  liftIO $ mapM_ (createDirectoryIfMissing True . inFolder) ["ById", "ByRank"]
  outputFormat <- lift . lift $ asks format
  let path to = inFolder to <.> extension outputFormat
  let csvLine mode t = liftIO . withFile (inFolder "metadata.csv") mode $
                         flip hPutStrLn (intercalate "," t)
  let createArticle n =
        do
          document@(Document {docId, docHead}) <- await
          let byId = path $ "ById" </> docId
          liftIO $ withFile byId WriteMode (writeOutput outputFormat document)
          liftIO . createLink byId $ path ("ByRank" </> show n)
          csvLine AppendMode [docId, show n, docHead]
          createArticle (n+1)
  csvLine WriteMode ["id", "rank", "head"]
  createArticle (1 :: Int)

main :: IO ()
main = do
  config@(Config {input, step}) <- Config.get
  context <- Config.getContext config
  ro config $ case step of
    Articles -> filterLogs Nothing
      (files input >>= (ro context . runEffect . (>-> createArticles) . articles))
    Fixed -> (ro context . fixed) `parallel` input
    Raw -> raw `parallel` input
  where
    files StdIn = liftIO (lines <$> getContents)
    files (FileInput filePath) = return [filePath]
    parallel f input = files input >>= mapM_ (runOn f)
    ro = flip runReaderT
