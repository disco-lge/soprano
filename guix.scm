(use-modules ((gnu packages haskell-xyz) #:select (ghc-edit-distance
                                                   ghc-optparse-applicative
                                                   ghc-pipes
                                                   ghc-roman-numerals
                                                   ghc-xml))
             ((guix build-system haskell) #:select (haskell-build-system))
             ((guix gexp) #:select (local-file))
             ((guix git-download) #:select (git-predicate))
             ((guix licenses) #:select (bsd-3))
             ((guix packages) #:select (package)))

(let
  ((%source-dir (dirname (current-filename))))
  (package
    (name "soprano")
    (version "devel")
    (source
      (local-file %source-dir
                  #:recursive? #t
                  #:select? (git-predicate %source-dir)))
    (build-system haskell-build-system)
    (inputs (list ghc-edit-distance ghc-pipes ghc-roman-numerals ghc-xml
                  ghc-optparse-applicative))
    (home-page "https://gitlab.huma-num.fr/disco-lge/soprano")
    (synopsis "A tool to extract textual content from ALTO files")
    (description
     "Soprano is a tool developed for project GÉODE
(@xref{https://geode-project.github.io/}) which studies the geographic discourse
in encyclopedias. It was desiged to extract encyclopedia articles from OCRed
pages represented by a set of ALTO files.

It lets one apply various filtering in the process, for instance specifying the
type of ALTO blocks to retain or setting a quality threshold on OCR recognition.
Articles can be output in raw text or encoded in XML-TEI.")
    (license bsd-3)))
