{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
module Context (
      Context(..)
    , HasContext
  ) where

import Control.Monad.Reader (MonadReader)
import Data.Set (Set)
import Scoria (Scoriae)
import Text.XML.Light.ALTO.Block.Pre (BlockType)

data Context = Context {
      denyList :: Scoriae
    , lineHeight :: Int
    , charWidth :: Int
    , noiseSet :: Set Char
    , keep :: Set BlockType
    , scoriaThreshold :: Float
    , titlePrefix :: Maybe String
  }

type HasContext m = MonadReader Context m
