module Data.Char.Extra (
      SimilarityClass(..)
    , castOn
    , couldBe
    , dashy
    , digit
    , romanDigit
    , similar
    , spots
    , upperCase
  ) where

import Data.Char (isNumber, isUpper)
import Data.List (foldl')
import Data.Map as Map (alter, empty, lookup)
import Data.Set as Set (fromList, member, union)

newtype SimilarityClass = SimilarityClass ((Char -> Bool), [Char])

digit :: SimilarityClass
digit = SimilarityClass (isNumber, ['0'..'9'])

romanDigit :: SimilarityClass
romanDigit = let digits = "IVXLCDM" in SimilarityClass ((`elem` digits), digits)

upperCase :: SimilarityClass
upperCase = SimilarityClass (isUpper, ['A'..'Z'])

couldBe :: SimilarityClass -> Char -> Bool
couldBe (SimilarityClass (test, witnesses)) c =
  test c || any (similar c) witnesses

castOn :: SimilarityClass -> Char -> Maybe Char
castOn (SimilarityClass (test, witnesses)) c
  | test c = Just c
  | otherwise = aux witnesses
  where
    aux "" = Nothing
    aux (target:targets)
      | similar target c = Just target
      | otherwise = aux targets

similar :: Char -> Char -> Bool
similar a b = a == b || (maybe False (Set.member b) $ Map.lookup a adjacence)
  where
    classes = [
          ovoid
        , smallOvoid
        , dashes
        , eightShaped
        , flagShaped
        , hooked
        , hookedDown
        , triangular
        , vertical
        , slantedRight
        , slantedLeft
        , openThing
        , closeThing
        , hShaped
        , twoShaped
        , crosses
        , cups
        , spots
      ]
    adjacence = foldl' pushClass Map.empty classes
    pushClass tmpMap charClass = foldl' (pushChar charClass) tmpMap charClass
    pushChar charClass tmpMap c = Map.alter (addToSet charClass) c tmpMap
    addToSet charClass existing =
      let charSet = Set.fromList charClass in
      Just $ maybe charSet (Set.union charSet) existing

dashy :: Char -> Bool
dashy = (`elem` dashes)

ovoid :: String
ovoid = "CGOQU0@()"

smallOvoid :: String
smallOvoid = "ceou©°"

dashes :: String
dashes = ".-—_~"

eightShaped :: String
eightShaped = "BS358$?6"

flagShaped :: String
flagShaped = "BEFPR"

hooked :: String
hooked = "Jgjqy9"

hookedDown :: String
hookedDown = "6b"

triangular :: String
triangular = "A4"

vertical :: String
vertical = "Iijl1!|"

slantedRight :: String
slantedRight = "%/" ++ vertical

slantedLeft :: String
slantedLeft = "\\" ++ vertical

openThing :: String
openThing = "[({"

closeThing :: String
closeThing = "])}"

hShaped :: String
hShaped = "HMNW"

twoShaped :: String
twoShaped = "Zz2"

crosses :: String
crosses = "HXYxy"

cups :: String
cups = "UVXYuvxy"

spots :: String
spots = ".:;,'`"
