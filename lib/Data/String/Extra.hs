module Data.String.Extra (
      ocrDistance
    , ucFirst
    , wordGroups
  ) where

import Data.Char (toUpper, toLower)
import Data.Char.Extra (dashy, similar, spots)
import Data.List (findIndex)
import Text.EditDistance (
    Costs(..), EditCosts(..), defaultEditCosts, levenshteinDistance
  )

wordGroups :: [String] -> [[String]]
wordGroups l =
  case findIndex (all dashy) l of
    Nothing -> [l]
    Just i -> let (before, after) = splitAt i l in
              before:(wordGroups $ drop 1 after)

ucFirst :: String -> String
ucFirst "" = ""
ucFirst (firstLetter:otherLetters) = (toUpper firstLetter):(toLower <$> otherLetters)

ocrDistance :: String -> String -> Float
ocrDistance sa sb =
  fromIntegral (levenshteinDistance ocrCosts sa sb) / fromIntegral defaultCost
  where
    defaultCost = 10
    cost (ca, cb) = if similar ca cb then 2 else defaultCost
    excuseSpots c = if c `elem` spots then 2 else defaultCost
    ocrCosts = defaultEditCosts {
          deletionCosts = VariableCost excuseSpots
        , insertionCosts = VariableCost excuseSpots
        , substitutionCosts = VariableCost cost
        , transpositionCosts = ConstantCost defaultCost
      }
