{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Log (
      MonadLogger(..)
    , LoggerT(..)
    , Logger(..)
    , LogLevel(..)
    , handle
    , io
  ) where

import Control.Applicative (liftA2)
import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Trans (MonadTrans(..))
import Control.Monad.Reader (MonadReader(..))
import Prelude hiding (log)

data LogLevel = Error | Warning | Info | Debug deriving (Enum, Eq, Ord, Read, Show)

data Logger m = Logger {
      level :: LogLevel
    , output :: String -> m ()
  }

class Monad m => MonadLogger m where
  fatal :: String -> m a
  log :: LogLevel -> String -> m ()

instance {-# OVERLAPPABLE #-} (MonadTrans t, Monad (t m), MonadLogger m) => MonadLogger (t m) where
  fatal = lift . fatal
  log lvl = lift . log lvl

instance MonadLogger IO where
  fatal = error
  log _ = putStrLn

newtype LoggerT m a = LoggerT {
    runLoggerT :: Logger m -> m (Either String a)
  }

instance Functor m => Functor (LoggerT m) where
  fmap f (LoggerT mA) = LoggerT $ fmap (fmap f) . mA

instance Applicative m => Applicative (LoggerT m) where
  pure a = LoggerT $ \_ -> pure (Right a)

  (<*>) (LoggerT mF) (LoggerT mA) =
    LoggerT $ \logger -> (liftA2 (<*>) (mF logger)) $ mA logger

instance Monad m => Monad (LoggerT m) where
  (>>=) (LoggerT mA) f = LoggerT $ \logger -> (>>= bindToM logger) (mA logger)
    where
      bindToM l = either (pure . Left) (flip runLoggerT l . f)

instance {-# OVERLAPS #-} Monad m => MonadLogger (LoggerT m) where
  fatal errMsg = LoggerT $ \_ -> pure (Left errMsg)

  log msgLevel msg =
    LoggerT $ \(Logger {level, output}) ->
      Right <$> (if msgLevel <= level then output msg else return ())

instance MonadTrans LoggerT where
  lift mA = LoggerT $ \_ -> (mA >>= pure . Right)

instance MonadIO m => MonadIO (LoggerT m) where
  liftIO mA = lift (liftIO mA)

instance MonadReader r m => MonadReader r (LoggerT m) where
  ask = lift $ ask
  local f (LoggerT mA) = LoggerT $ local f . mA

io :: MonadIO m => LogLevel -> Logger m
io level = Logger {
      level
    , output = liftIO . putStrLn
  }

handle :: Monad m => LoggerT m a -> Logger m -> (String -> m a) -> m a
handle loggerT logger errorHandler =
  runLoggerT loggerT logger >>= either errorHandler return
