module Scoria (
      Scoriae
    , fromCSV
  ) where

import Control.Monad.IO.Class (MonadIO(..))
import Data.Set (Set, fromList)

type Scoriae = Set String

fromCSV :: MonadIO m => FilePath -> m Scoriae
fromCSV = liftIO . fmap (fromList . drop 1 . lines) . readFile
