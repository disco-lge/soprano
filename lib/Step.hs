{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Step (
      Output(..)
    , articles
    , fixed
    , raw
  ) where

import Context (Context(..), HasContext)
import Control.Monad ((>=>))
import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Reader (asks)
import Data.Foldable (toList)
import Data.Set as Set (member)
import Log (MonadLogger(..))
import Pipes ((>->), Producer, for, each, yield)
import Text.XML.Light (Content, parseXML, showContent)
import Text.XML.Light.ALTO as ALTO (PrintSpace(..), edit, open, updateBlocks)
import Text.XML.Light.ALTO.Block as Block (Block(..), characterize)
import Text.XML.Light.ALTO.Clean (fitBlock, text)
import Text.XML.Light.ALTO.Dump as ALTO (dumpText)
import Text.XML.Light.ALTO.Layout as Layout (arrange)
import Text.XML.Light.ALTO.RectElement (RectElement)
import Text.XML.Light.ALTO.Retrieve as Retrieve (blocks)
import Text.XML.Light.TEI as TEI(Document, dumpText, encode)
import Text.XML.Light.TEI.Article (segment)
import Utils (skim)

type Stream a m r = Producer (Maybe a) m r

raw :: MonadIO m => FilePath -> m [Content]
raw = fmap parseXML . liftIO . readFile

fix :: (HasContext m, MonadLogger m) => [RectElement] -> m [Block]
fix =
    skim text -- delete empty blocks, apply explicit scoriae and noise filtering
  >=> mapM fitBlock -- adjust each element's geometry to its content
  >=> Retrieve.blocks -- some blocks got fusioned by the OCR, pop them out
  >=> Layout.arrange -- arrange the elements in the list into a tree layout
  >=> Block.characterize -- enrich each element with a block type and rework the layout a bit
  >=> filterBlocks . toList

fixed :: (HasContext m, MonadIO m, MonadLogger m) => FilePath -> m [Content]
fixed = raw >=> ALTO.edit (updateBlocks (fix >=> return . fmap blockElement))

filterBlocks :: HasContext m => [Block] -> m [Block]
filterBlocks input =
  (`filter` input) <$> asks (filterBlock . flip Set.member . keep)
  where
    filterBlock test (Block {blockType}) = test blockType
    filterBlock _ _ = False

articles :: (HasContext m, MonadIO m, MonadLogger m) => [FilePath] -> Producer Document m ()
articles filePaths = getBlocks >-> segment
  where
    getBlocks :: (HasContext m, MonadIO m, MonadLogger m) => Stream Block m ()
    getBlocks = do
      for (each $ zip [0..] filePaths) $ \(i, filePath) -> do
        yield . Just $ PageBreak i
        raw filePath >>= ALTO.open >>= fix . rectElements >>= each . fmap Just
      yield Nothing

class Output t where
  toTxt :: t -> [String]
  toXML :: t -> [String]

instance Output [Content] where
  toTxt = ALTO.dumpText
  toXML = fmap showContent

instance Output Document where
  toTxt = TEI.dumpText
  toXML = toXML . encode
