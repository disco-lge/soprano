{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
module Text.XML.Light.ALTO (
      PrintSpace(..)
    , close
    , edit
    , open
    , updateBlocks
  ) where

import Data.Foldable (foldrM)
import Log (MonadLogger(..), LogLevel(..))
import Prelude hiding (log)
import Text.Printf (printf)
import Text.XML.Light (Content(..), Element(..))
import Text.XML.Light.ALTO.Geometry (Rectangular(..))
import Text.XML.Light.ALTO.RectElement (RectElement(..), rectangle)
import Text.XML.Light.Cursor (Cursor(..), fromForest, setContent, toForest)
import Text.XML.Light.Extra (getAttr, indent, getTag)
import Text.XML.Light.XPath (t, xPathCursor)

data PrintSpace = PrintSpace {
      rectElements :: [RectElement]
    , printSpaceElement :: RectElement
    , cursor :: Cursor
  } deriving Show

instance Rectangular PrintSpace where
  getRectangle = getRectangle . printSpaceElement

open :: MonadLogger m => [Content] -> m PrintSpace
open xml =
  case xPathCursor printSpaceXPath =<< fromForest xml of
    Just cursor@(Cur {current = Elem e@(Element {elContent})}) -> do
      printSpaceElement <- getRectElement e
      rectElements <- foldrM keepRectElement [] elContent
      return $
        PrintSpace {rectElements, printSpaceElement, cursor}
    _ -> fatal "PrintSpace element not found"
  where
    printSpaceXPath = t<$>["alto", "Layout", "Page", "PrintSpace"]
    getRectElement e =
      either (fatal . missingGeometry e) return $ rectangle e
    missingGeometry el m =
      printf "Missing geometry in %s element%s (%s)"
        (getTag el) (maybe "" (' ':) (getAttr "ID" el)) m
    keepRectElement (Elem e) l =
      case rectangle e of
        Left message -> log Warning (missingGeometry e message) >> return l
        Right rectElement -> return (rectElement:l)
    keepRectElement _ l = return l

close :: Monad m => PrintSpace -> m [Content]
close (PrintSpace {rectElements, printSpaceElement, cursor}) =
  let elContent = indent 4 (Elem . xml <$> rectElements) in
  return . toForest $
    setContent (Elem ((xml printSpaceElement) {elContent})) cursor

edit :: MonadLogger m => (PrintSpace -> m PrintSpace) -> [Content] -> m [Content]
edit f xml = open xml >>= f >>= close

updateBlocks :: MonadLogger m =>
  ([RectElement] -> m [RectElement]) -> PrintSpace -> m PrintSpace
updateBlocks editor pS@(PrintSpace {rectElements}) = do
  newRectElements <- editor rectElements
  return $ pS {rectElements = newRectElements}
