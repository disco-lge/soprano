{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
module Text.XML.Light.ALTO.Block (
      Block(..)
    , characterize
    , defaultHorizontalBody
    , glueColumns
    , scoriae
  ) where

import Context (Context(..), HasContext)
import Control.Monad.Reader (asks)
import Data.Foldable (foldrM)
import Log (MonadLogger(..), LogLevel(..))
import Prelude hiding (log)
import Text.Printf (printf)
import Text.XML.Light (Element(..))
import Text.XML.Light.ALTO.Block.Peritext (PageRows(..), separate)
import Text.XML.Light.ALTO.Block.Pre (BlockType(..), sumUp)
import Text.XML.Light.ALTO.Dump (certainty)
import Text.XML.Light.ALTO.Geometry (
      Axis(..), Rectangular(..), SegmentOrdering(..), aligned, anchored, bottom
    , contains, get, hMiddle, horizontally, left, right, sides, connected, top
    , vPos, vMax, width
  )
import Text.XML.Light.ALTO.Layout as Layout (
    Layout(..), append, array, mergeInto, tree, update
  )
import Text.XML.Light.ALTO.RectElement as RectElement (
    RectElement(..), detectGeometry, getLines, identify
  )
import Text.XML.Light.Extra (getTag)
import Text.XML.Light.XPath (t, xPath)
import Utils ((.&.), (.|.), skim)

data Block =
    Block {
        blockType :: BlockType
      , blockElement :: RectElement
    }
  | PageBreak Int

instance Rectangular Block where
  getRectangle = getRectangle . blockElement

instance Show Block where
  show (Block {blockType, blockElement})
    | getTag (xml blockElement) == "TextBlock" =
      printf "\"%s\" (%s)" (sumUp blockElement) (show blockType)
    | otherwise = printf "[%s]" $ identify blockElement
  show (PageBreak n) = printf "[page %d]" n

blocks :: Functor f => BlockType -> f RectElement -> f Block
blocks blockType = fmap (\blockElement -> Block {blockElement, blockType})

tee :: MonadLogger m => (a -> m ()) -> a -> m a
tee display a = display a >> return a

characterize :: (HasContext m, MonadLogger m) => Layout RectElement -> m (Layout Block)
characterize layout = aux layout >>= tee (log Info . tree)
  where
    aux (Array {axis, layouts})
      | axis == V = -- we expect a page to be a vertical array of blocks
        separate layouts >>= organize
      | otherwise = organize $ PageRows [] Nothing layouts Nothing [] -- maybe the row is the body ? let's decide later
    aux l = organize $ PageRows [] Nothing [l] Nothing [] -- can't say anything, let's just say it's the body and decide later

organize :: (HasContext m, MonadLogger m) => PageRows RectElement -> m (Layout Block)
organize (PageRows {beforeRows, headRow, middleRows, footRow, afterRows}) = do
  topRows <- makeSpecial beforeRows
  headRows <- maybe noHeader (return . pure . blocks Header) headRow
  bodyRows <- categorizeBody =<< withoutScoriae middleRows
  let footRows = maybe [] (pure . blocks Footer) footRow
  bottomRows <- makeSpecial afterRows
  return . array V $ topRows ++ headRows ++ bodyRows ++ footRows ++ bottomRows
  where
    makeSpecial = fmap (blocks Special <$>) . withoutScoriae
    noHeader = log Info "No header candidates found" >> return []
    withoutScoriae = skim (update scoriae)

defaultHorizontalBody :: (Rectangular a, HasContext m, MonadLogger m) =>
  [Layout a] -> m [Layout a]
defaultHorizontalBody body = foldrM glueColumns [] body

glueColumns :: (Rectangular a, HasContext m, MonadLogger m) =>
  Layout a -> [Layout a] -> m [Layout a]
glueColumns s@(Simple _) l@((Array {axis = H, layouts}):others) = return $
  maybe (s:l) (:others) $ mergeInto V layouts [s]
glueColumns a@(Array {axis = H, layouts}) l@(s@(Simple _):others) = return $
  maybe (a:l) (:others) $ mergeInto V layouts [s]
glueColumns a@(Array {axis = H}) l@(b@(Array {axis = H}):others) = do
  lineHeight <- asks Context.lineHeight
  let dv = get vPos b - get vMax a
  return $
    if dv > 3*lineHeight && flat (layouts a) (layouts b) lineHeight
    then (a:l)
    else maybe (a:l) (:others) $ mergeInto V (layouts a) (layouts b)
  where
    vSides = let (minSide, maxSide) = sides V in [minSide, maxSide]
    flat aLayouts bLayouts lineHeight =
      all (aligned lineHeight) $ zip vSides [bLayouts, aLayouts]
glueColumns a l@(b:others)
  | connected V a b = return (a:b:others)
  | horizontally [SLT] a b = return (append H a b:others)
  | horizontally [SGT] a b = return (append H b a:others)
  | otherwise = return (a:l)
glueColumns a [] = return [a]

countLines :: Layout RectElement -> Int
countLines = foldr ((+) . length . getLines) 0

categorizeBody :: (HasContext m, MonadLogger m) => [Layout RectElement] -> m [Layout Block]
categorizeBody [l@(Simple _)] =
  return [blocks (if countLines l > 5 then Text else Special) l]
categorizeBody body = mapM categorizeRow =<< defaultHorizontalBody body
  where
    bodyRect = detectGeometry body

    categorizeRow row@(Simple r)
      | (anchored bodyRect left .&. anchored bodyRect right) row
        && countLines row > 3 = return $ blocks Text row
      | r `contains` (hMiddle bodyRect) $ H = return $ blocks Caption row
    categorizeRow (Array {axis = H, layouts, Layout.geometry}) = do
      newLayouts <- mapM (categorizeColumn geometry) layouts
      return $ Array {axis = H, layouts = newLayouts, Layout.geometry}
    categorizeRow row = return $ blocks Special row

    categorizeColumn rect column
      | (anchored rect top .|. anchored rect bottom) column
        && countLines column >= 2 =
          categorizeTextColumn column
      | otherwise = return $ blocks Special column

    categorizeTextColumn (Array {axis = V, layouts, Layout.geometry}) = do
      newLayouts <- mapM (categorizeTextBlock geometry) layouts
      return $ Array {axis = V, layouts = newLayouts, Layout.geometry}
    categorizeTextColumn column@(Simple _) = return $ blocks Text column
    categorizeTextColumn column = return $ blocks Special column

    categorizeTextBlock rect block@(Simple _)
      | (anchored rect left .|. anchored rect right ) block
        && 2*(get width block) >= width rect = return $ blocks Text block
    categorizeTextBlock _ block = return $ blocks Special block

scoriae :: (HasContext m, MonadLogger m) => RectElement -> m (Maybe RectElement)
scoriae rectElement@(RectElement {xml})
  | getTag xml == "TextBlock" = do
    isLow <- asks (flip (<) . scoriaThreshold)
    case xPath (t<$>["TextLine", "String"]) $ elContent xml of
      l
        | all (certainty isLow) l -> log Warning deleted >> return Nothing
      _ -> return $ Just rectElement
  | otherwise = return $ Just rectElement
  where
    deleted = printf "Deleting scoria %s" (identify rectElement)
