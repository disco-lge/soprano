module Text.XML.Light.ALTO.Block.Caption (
    possibly
  ) where

import Text.XML.Light.ALTO.Dump (lineWords)
import Text.XML.Light.ALTO.RectElement (RectElement(..))

possibly :: RectElement -> Bool
possibly rectElement =
  case lineWords $ xml rectElement of
    [] -> False
    (firstWord:_) -> all id $ zipWith couldBe "Fig." firstWord
  where
    couldBe 'F' = (`elem` "FEPBR")
    couldBe 'i' = (`elem` "ijl1")
    couldBe 'g' = (`elem` "gqj")
    couldBe '.' = (`elem` ".:;,")
    couldBe c = (== c)
