{-# LANGUAGE NamedFieldPuns #-}
module Text.XML.Light.ALTO.Block.Header (
      possibly
    , rate
  ) where

import Data.Char (isSpace, isUpper)
import Data.Char.Extra (couldBe, digit, dashy)
import Data.String.Extra (wordGroups)
import Text.XML.Light.ALTO.Dump (dumpLine, lineWords)
import Text.XML.Light.ALTO.Geometry (Axis(..))
import Text.XML.Light.ALTO.Layout as Layout (Layout(..))
import Text.XML.Light.ALTO.RectElement (RectElement(..), getLines)
import Utils ((.|.), mostly)

check :: RectElement -> Bool
check eElem =
  case getLines eElem of
    [firstLine] -> all peritextSet $ dumpLine firstLine
    _ -> False

peritextSet :: Char -> Bool
peritextSet = isUpper .|. isSpace .|. dashy .|. (couldBe digit) .|. punctuation
  where
    punctuation = (`elem` "'’")

possibly :: RectElement -> Bool
possibly rectElement =
    not (null groups) && maximum (length <$> groups) < 5
  && mostly peritextSet (concat strings)
  where
    strings = lineWords $ xml rectElement
    groups = wordGroups strings

rate :: Layout RectElement -> Float
rate (Simple a)
  | check a = 2
rate (Array {axis = H, layouts})
  | count candidates > 0 && count layouts < 4 =
    (count candidates  / count layouts) + 2 - abs (2 - count layouts)
  where
    count = (fromIntegral . length :: [a] -> Float)
    candidates = [x | Simple x <- layouts, check x]
rate _ = 0
