{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DeriveFunctor #-}
module Text.XML.Light.ALTO.Block.Peritext (
      PageRows(..)
    , dumpPageRows
    , separate
  ) where

import Control.Monad.State (MonadState(..), evalStateT)
import Data.Function (on)
import Data.List (maximumBy)
import Log (MonadLogger(..), LogLevel(..))
import Prelude hiding (log)
import Text.Printf (printf)
import Text.XML.Light.ALTO.Block.Header as Header (rate)
import Text.XML.Light.ALTO.Block.Pre (sumUp)
import Text.XML.Light.ALTO.Block.Sixteenmo as Sixteenmo (rate)
import Text.XML.Light.ALTO.Layout as Layout (Layout(..), tree)
import Text.XML.Light.ALTO.RectElement (RectElement(..))

data PageRows a = PageRows {
      beforeRows :: [Layout a]
    , headRow :: Maybe (Layout a)
    , middleRows :: [Layout a]
    , footRow :: Maybe (Layout a)
    , afterRows :: [Layout a]
  } deriving (Functor, Show)


dumpPageRows :: (MonadLogger m, Show b) => (a -> b) -> PageRows a -> m ()
dumpPageRows f (PageRows {
    beforeRows, headRow, middleRows, footRow, afterRows
  }) = do
  log Debug "Above" *> mapM_ (log Debug . tree . fmap f) beforeRows
  log Debug "Header" *> mapM_ (log Debug . tree . fmap f) headRow
  log Debug "Body" *> mapM_ (log Debug . tree . fmap f) middleRows
  log Debug "Footer" *> mapM_ (log Debug . tree . fmap f) footRow
  log Debug "Under" *> mapM_ (log Debug . tree . fmap f) afterRows

separate :: MonadLogger m => [Layout RectElement] -> m (PageRows RectElement)
separate [] = return $ PageRows [] Nothing [] Nothing []
separate rows = flip evalStateT rows $ do
  (beforeRows, headRow) <- if hMax > 0
                           then (,) <$> pop hI <*> (Just . head <$> pop 1)
                           else return ([], Nothing)
  (middleRows, footRow) <- if fMax > 0
                           then (,) <$> pop (fI - hI - 1) <*> (Just . head <$> pop 1)
                           else (,) <$> state (flip (,) []) <*> (return Nothing)
  log Debug $ printf "Found %s and %s" (showM False headRow) (showM True footRow)
  afterRows <- get
  return $ PageRows {beforeRows, headRow, middleRows, footRow, afterRows}
  where
    headerDistribution = zip [0..] $ Header.rate <$> rows
    footerDistribution = zip [0..] $ Sixteenmo.rate <$> rows
    grade ((_, a), (_, b)) = a + b + if a * b > 0 then 1 else 0
    combinations =
      [(h, f) | h@(i, _) <- headerDistribution, f <- drop (i+1) footerDistribution]
    ((hI, hMax), (fI, fMax)) = maximumBy (compare `on` grade) combinations
    pop = state . splitAt
    showM False = maybe "" (\s -> printf "header %s (#%d)" (tree $ sumUp <$> s) hI)
    showM True = maybe "" (\s -> printf "footer %s (#%d)" (tree $ sumUp <$> s) fI)
