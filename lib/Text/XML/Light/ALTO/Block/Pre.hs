{-# LANGUAGE FlexibleInstances #-}
module Text.XML.Light.ALTO.Block.Pre (
      BlockType(..)
    , oneLine
    , report
    , sumUp
  ) where

import Data.Set as Set (Set, fromList)
import Data.String.Extra (ucFirst)
import Log (MonadLogger(..), LogLevel(..))
import Prelude hiding (log)
import Text.ParserCombinators.ReadP (char, eof, munch, sepBy)
import Text.ParserCombinators.ReadPrec (lift)
import Text.Printf (printf)
import Text.Read (readPrec)
import Text.XML.Light (Element(..))
import Text.XML.Light.ALTO.Dump (dumpLine)
import Text.XML.Light.ALTO.RectElement (RectElement(..), getRectLines)
import Text.XML.Light.XPath (t, xPath)

data BlockType =
      Header
    | Footer
    | Text
    | Caption
    | Special
    deriving (Enum, Eq, Ord, Read, Show)

instance {-# OVERLAPPING #-} Read (Set BlockType) where
  readPrec = Set.fromList <$> lift
    (((read . ucFirst <$> munch (/= ',')) `sepBy` (char ',')) <* eof)

sumUp :: RectElement -> String
sumUp textBlockElement = firstLine (dumpLine <$> xPath [t"TextLine"] contents)
  where
    contents = elContent $ xml textBlockElement
    firstLine l = if null l then "" else ellipsis (head l)
    ellipsis s = if length s > 30 then (take 29 s ++ "…") else s

report :: MonadLogger m => String -> RectElement -> m ()
report kind line =
  mapM_ (log Info) (printf "Retrieved a probable %s :" kind:[dumpLine $ xml line])

oneLine :: (RectElement -> Bool) -> RectElement -> Bool
oneLine p textBlockElement =
  case getRectLines textBlockElement of
    [onlyLine] -> p onlyLine
    _ -> False
