{-# LANGUAGE NamedFieldPuns #-}
module Text.XML.Light.ALTO.Block.Sixteenmo (
      check
    , numbering
    , rate
    , textual
  ) where

import Data.Char (toUpper)
import Data.Char.Extra (couldBe, digit, romanDigit)
import Data.List (findIndex)
import Data.String.Extra (ocrDistance, wordGroups)
import Log (MonadLogger(..))
import Prelude hiding (log)
import Text.XML.Light.ALTO.Block.Pre (oneLine, report)
import Text.XML.Light.ALTO.Dump (lineWords)
import Text.XML.Light.ALTO.Geometry (Axis(..))
import Text.XML.Light.ALTO.Layout (Layout(..))
import Text.XML.Light.ALTO.RectElement (
    RectElement(..), getRectLines, setChildren, textBlock
  )
import Utils (mostly)

lastLine :: MonadLogger m =>
  String -> (RectElement -> Bool) -> RectElement -> m ([RectElement], RectElement)
lastLine kind p rectElement =
  let rectLines = getRectLines rectElement in
  case splitAt (length rectLines - 1) rectLines of
    (otherLines@(_:_), [line])
      | p line ->
        report kind line
        >> return ([textBlock [line]], setChildren otherLines rectElement)
    _ -> return ([], rectElement)

textual :: MonadLogger m => RectElement -> m ([RectElement], RectElement)
textual = lastLine "textual 16mo" isTextual16mo

isTextual16mo :: RectElement -> Bool
isTextual16mo line =
  case fmap (fmap toUpper . concat) . wordGroups . lineWords $ xml line of
    (fstBlock:end)
      | ocrDistance fstBlock target < 5 ->
        case end of
          (midBlock:_) -> (couldBe romanDigit) `mostly` midBlock
          _ -> True
    _ -> False
  where
    target = "GRANDEENCYCLOPÉDIE"

check :: RectElement -> Bool
check rectElement =
  let rectLines = getRectLines rectElement in
  length rectLines == 1 && isTextual16mo (rectLines !! 0)

numbering :: MonadLogger m => RectElement -> m ([RectElement], RectElement)
numbering = lastLine "16mo folio number" isFolioNumber

isFolioNumber :: RectElement -> Bool
isFolioNumber line =
  case lineWords $ xml line of
    [word] -> length word < 5 && (couldBe digit) `mostly` word
    _ -> False

rate :: Layout RectElement -> Float
rate (Array {axis = H, layouts}) =
  let rects = [x | Simple x <- layouts] in maybe 0 id $ do
    i0 <- findIndex (oneLine isTextual16mo) rects
    Just . maybe 2 (\_ -> 4) $ findIndex (oneLine isFolioNumber) (drop (i0+1) rects)
  where
rate _ = 0
