{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
module Text.XML.Light.ALTO.Clean (
      fitBlock
    , text
  ) where

import Context (Context(..), HasContext)
import Control.Monad.Reader (asks)
import Data.Maybe (catMaybes)
import Data.Set (member)
import Log (MonadLogger(..), LogLevel(..))
import Prelude hiding (log)
import Text.Printf (printf)
import Text.XML.Light (Content(..), Element(..))
import Text.XML.Light.ALTO.Geometry (get, height, vPos)
import Text.XML.Light.ALTO.RectElement (
    RectElement(..), identify, rectangle, setChildren, setGeometry
  )
import Text.XML.Light.Extra (getAttr, setAttr, getTag)
import Text.XML.Light.XPath (childElements)

filterContent :: HasContext m => Content -> m (Maybe Content)
filterContent (Elem element) = fmap Elem <$> filterElement element
filterContent _ = return Nothing

filterElement :: HasContext m => Element -> m (Maybe Element)
filterElement element@(Element {elContent})
  | getTag element == "String" = filterString element
  | getTag element == "SP" = return Nothing
  | getTag element `elem` ["TextBlock", "TextLine"] = do
    filtered <- mapM filterContent elContent
    return $ case catMaybes filtered of
      [] -> Nothing
      l -> Just $ element {elContent = l}
  | otherwise = return $ Just element

filterString :: HasContext m => Element -> m (Maybe Element)
filterString element = asks filterWith
  where
    trim = filter (not . (`elem` "\t\n\r "))
    editElem s =
      if null $ trim s then Nothing else Just $ setAttr ("CONTENT", s) element
    filterWith (Context {denyList, noiseSet})
      | maybe False (`member` denyList) $ getAttr "ID" element = Nothing
      | otherwise =
        getAttr "CONTENT" element >>= editElem . filter (not . (`member` noiseSet))

text :: (HasContext m, MonadLogger m) => RectElement -> m (Maybe RectElement)
text rectElement@(RectElement {xml}) = filterElement xml >>= output
  where
    deleted = printf "Deleting empty %s" (identify rectElement)
    output Nothing = log Warning deleted >> return Nothing
    output (Just newElement) = return . Just $ rectElement {xml = newElement}

updateGeometry :: MonadLogger m => RectElement -> [RectElement] -> m RectElement
updateGeometry rectElement [] =
  log Debug "No elements to infer geometry, keeping as is" >> return rectElement
updateGeometry rectElement@(RectElement {xml}) children = do
  let newRectElement = setChildren consolidated rectElement
  let newGeometry = geometry newRectElement
  if newGeometry /= geometry rectElement
  then log Debug $ debug newGeometry
  else return ()
  return newRectElement
  where
    consolidated =
      (if getTag xml == "TextLine" then fmap consolidate else id) children
    consolidate child@(RectElement {geometry})
      | vPos geometry == 0 = flip setGeometry child $ geometry {
              vPos = get vPos rectElement
            , height = get height rectElement
          }
      | otherwise = child
    debug rect =
      printf "Adjusting %s geometry to %s" (identify rectElement) (show rect)

fitBlock :: MonadLogger m => RectElement -> m RectElement
fitBlock rectElement@(RectElement {xml})
  | getTag xml `elem` ["PrintSpace", "TextBlock", "TextLine"] =
     mapM fitBlock childRectangles >>= updateGeometry rectElement
  | otherwise = return rectElement
  where
    childRectangles =
      [rect | Right rect <- rectangle <$> childElements (elContent xml)]
