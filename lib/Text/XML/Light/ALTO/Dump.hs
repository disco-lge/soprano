module Text.XML.Light.ALTO.Dump (
      certainty
    , dumpLine
    , dumpString
    , dumpText
    , lineWords
  ) where

import Text.XML.Light (Content, Element(..))
import Text.XML.Light.Extra (getAttr)
import Text.XML.Light.XPath (t, xPath)

dumpText :: [Content] -> [String]
dumpText = fmap dumpLine . xPath pathToLines
  where
    pathToLines =
      t<$>["alto", "Layout", "Page", "PrintSpace", "TextBlock", "TextLine"]

dumpLine :: Element -> String
dumpLine = unwords . lineWords

lineWords :: Element -> [String]
lineWords e = xPath [t"String"] (elContent e) >>= dumpString

dumpString :: Element -> [String]
dumpString = maybe [] pure . getAttr "CONTENT"

certainty :: (Float -> Bool) -> Element -> Bool
certainty p = maybe False (p . read) . getAttr "WC"
