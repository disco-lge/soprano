module Text.XML.Light.ALTO.Geometry (
      Axis(..)
    , Rectangular(..)
    , Rectangle(..)
    , SegmentOrdering(..)
    , Side
    , above
    , after
    , aligned
    , anchored
    , before
    , bottom
    , connected
    , contains
    , get
    , hMax
    , hMiddle
    , horizontally
    , left
    , leftOf
    , other
    , right
    , scale
    , sides
    , top
    , vertically
    , vMax
    , vMiddle
  ) where

import Utils ((.&.))

data Axis = H | V deriving (Eq, Show)

other :: Axis -> Axis
other H = V
other _ = H

newtype Side = Side (Axis, Bool)

top :: Side
top = Side (V, False)

right :: Side
right = Side (H, True)

bottom :: Side
bottom = Side (V, True)

left :: Side
left = Side (H, False)

sides :: Axis -> (Side, Side)
sides V = (top, bottom)
sides H = (left, right)

data Rectangle = Rectangle {
      hPos :: Int
    , vPos :: Int
    , width :: Int
    , height :: Int
  } deriving (Eq, Show)

class Rectangular a where
  getRectangle :: a -> Rectangle

instance Rectangular Rectangle where
  getRectangle = id

instance Rectangular b => Rectangular (a, b) where
  getRectangle = getRectangle . snd

get :: Rectangular a => (Rectangle -> Int) -> a -> Int
get f = f . getRectangle

hMax :: Rectangle -> Int
hMax rectangle = hPos rectangle + width rectangle

vMax :: Rectangle -> Int
vMax rectangle = vPos rectangle + height rectangle

hMiddle :: Rectangle -> Int
hMiddle rectangle = hPos rectangle + (width rectangle `div` 2)

vMiddle :: Rectangle -> Int
vMiddle rectangle = vPos rectangle + (height rectangle `div` 2)

leftOf :: Rectangular a => a -> a -> Bool
leftOf a b = get hPos a < get hPos b

above :: Rectangular a => a -> a -> Bool
above a b = get vPos a < get vPos b

contains :: Rectangular a => a -> Int -> Axis -> Bool
contains a value H = get hPos a <= value && value <= get hMax a
contains a value V = get vPos a <= value && value <= get vMax a

data SegmentOrdering = SLT | LEQ | CS | CD | GEQ | SGT deriving (Eq, Show)

segmentOrdering :: Rectangular a => (a -> Int) -> (a -> Int) -> a -> a -> SegmentOrdering
segmentOrdering start end a b
  | minA < minB = if maxA < minB then SLT else if maxA < maxB then LEQ else CS
  | otherwise =  if maxB < minA then SGT else if maxA < maxB then CD else GEQ
  where
    (minA, maxA, minB, maxB) = (start a, end a, start b, end b)

hOrdering :: Rectangular a => a -> a -> SegmentOrdering
hOrdering = segmentOrdering (get hPos) (get hMax)

vOrdering :: Rectangular a => a -> a -> SegmentOrdering
vOrdering = segmentOrdering (get vPos) (get vMax)

horizontally :: Rectangular a => [SegmentOrdering] -> a -> a -> Bool
horizontally target a b = (hOrdering a b) `elem` target

vertically :: Rectangular a => [SegmentOrdering] -> a -> a -> Bool
vertically target a b = (vOrdering a b) `elem` target

after :: [SegmentOrdering]
after = [CS, GEQ, SGT]

before :: [SegmentOrdering]
before = [SLT, LEQ, CS]

scale :: Float -> Rectangle -> Rectangle
scale factor rectangle = Rectangle newHPos newVPos newWidth newHeight
  where
    reduce x dx =
      let dy = factor * fromIntegral dx in
      (round (fromIntegral x - dy/2), round dy)
    (newHPos, newWidth) = reduce (hMiddle rectangle) (width rectangle)
    (newVPos, newHeight) = reduce (vMiddle rectangle) (height rectangle)

anchored :: (Rectangular a, Rectangular b) => a -> Side -> b -> Bool
anchored reference (Side (axis, atEnd)) a =
  20 * abs (get pos a - get pos reference) < get measure a
  where
    (pos, measure)
      | axis == V = (if atEnd then vMax else vPos, height)
      | otherwise = (if atEnd then hMax else hPos, width)

connected :: Rectangular a => Axis -> a -> a -> Bool
connected axis a b =
  (anchored a minSide .&. anchored a maxSide) b || direction [CS, CD] a b
  where
    (direction, (minSide, maxSide)) =
      sides <$> if axis == V then (horizontally, H) else (vertically, V)

aligned :: Rectangular a => Int -> (Side, [a]) -> Bool
aligned _ (_, []) = True
aligned lineHeight (Side (axis, atEnd), a:as) = all (within $ measure a) as
  where
    measure = get (if axis == V
        then if atEnd then vMax else vPos
        else if atEnd then hMax else hPos)
    within ref x = abs (measure x - ref) <= lineHeight
