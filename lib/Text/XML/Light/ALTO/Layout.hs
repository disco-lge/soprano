{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
module Text.XML.Light.ALTO.Layout (
      Layout(..)
    , append
    , arrange
    , array
    , mergeInto
    , tree
    , update
  ) where

import Control.Monad.State (evalState)
import Data.List (find, group, sortOn)
import Log (MonadLogger(..), LogLevel(..))
import Prelude hiding (log)
import Text.XML.Light.ALTO.Geometry (
      Axis(..), Rectangle(..), Rectangular(..), SegmentOrdering(..), above
    , connected, get, hMax, hMiddle, horizontally, leftOf, other, scale
    , vertically, vMax, vMiddle
  )
import Text.XML.Light.ALTO.RectElement (detectGeometry)
import Utils (popMin, skim, takeAll)

data Layout a =
    Simple a
  | Array {
          axis :: Axis
        , layouts :: [Layout a]
        , geometry :: Rectangle
      } deriving Show

instance Rectangular a => Rectangular (Layout a) where
  getRectangle (Simple a) = getRectangle a
  getRectangle l = geometry l

instance Functor Layout where
  fmap f (Simple a) = Simple (f a)
  fmap f (Array {axis, layouts, geometry}) =
    Array {axis, layouts = fmap f <$> layouts, geometry}

instance Foldable Layout where
  foldMap f (Simple a) = f a
  foldMap f (Array {layouts}) = foldMap (foldMap f) layouts

tree :: Show a => Layout a -> String
tree = unlines . aux
  where
    indent = ('\t':)
    aux (Simple a) = [show a]
    aux (Array {axis, layouts}) =
      (if axis == V then "Column" else "Row"):(indent <$> (layouts >>= aux))

array :: Rectangular a => Axis -> [Layout a] -> Layout a
array axis layouts =
  let geometry = detectGeometry layouts in Array {axis, layouts, geometry}

arrange :: (Rectangular a, MonadLogger m) => [a] -> m (Layout a)
arrange = fmap normalize . arrangeRec V

arrangeRec :: (Rectangular a, MonadLogger m) => Axis -> [a] -> m (Layout a)
arrangeRec axis [] = do
  log Info "No elements to arrange" >> return (array axis [])
arrangeRec _ [rectElement] = return $ Simple rectElement
arrangeRec axis l =
  case group $ middle <$> l of
    [_] -> return . array axis $ Simple <$> sorted
    _ -> do
      (newAxis, strips) <- while tryFactor $ iterate (*0.9) 1
      mapM (arrangeRec (other newAxis)) strips >>= return . array newAxis
  where
    sorted = sortOn (get $ if axis == V then vPos else hPos) l
    middle r = (get hMiddle r, get vMiddle r)
    rectangles = getRectangle <$> l
    tryFactor f = arrangeIndices axis $ zip l (scale f <$> rectangles)

while :: MonadLogger m => (a -> Maybe b) -> [a] -> m b
while _ [] = fatal "Empty list"
while f (a:as) = maybe (while f as) return $ f a

arrangeIndices :: Rectangular b => Axis -> [(a, b)] -> Maybe (Axis, [[a]])
arrangeIndices axis assoc
  | length strips > 1 = -- successfully split the blocks along the expected axis
      Just (axis, fmap fst <$> strips)
  | length transposed > 1 = -- had to split the blocks along the other axis, whatever works
      Just (other axis, fmap fst <$> transposed)
  | otherwise = Nothing -- no luck, will retry with a stronger reduction factor
  where
    strips = getStrips axis assoc
    transposed = getStrips (other axis) assoc

getStrips :: Rectangular a => Axis -> [a] -> [[a]]
getStrips axis = evalState getStripsAux
  where
    getStripsAux = do
      next <- popMin (\_-> True) (if axis == V then above else leftOf)
      case next of
        Nothing -> return []
        Just firstElem -> (:) <$> getStrip [firstElem] <*> getStripsAux
    getStrip tmpStrip = do
      let tmpGeometry = detectGeometry tmpStrip
      nextElems <- takeAll (besides tmpGeometry)
      case nextElems of
        [] -> return tmpStrip
        _ -> getStrip (nextElems ++ tmpStrip)
    besides a
      | axis == V = \b -> get vPos b < get vMax a
      | otherwise = \b -> get hPos b < get hMax a

normalize :: Rectangular a => Layout a -> Layout a
normalize layout@(Simple _) = layout
normalize layout@(Array {axis}) =
  layout {layouts = expand axis . fmap normalize $ layouts layout}

append :: Rectangular a => Axis -> Layout a -> Layout a -> Layout a
append orientation aLayout bLayout =
  Array orientation (expand orientation sorted) (detectGeometry sorted)
  where
    sorted =
      sortOn (get $ if orientation == V then vPos else hPos) [aLayout, bLayout]

expand :: Axis -> [Layout a] -> [Layout a]
expand orientation = foldr expander []
  where
    expander s@(Simple _) = (s:)
    expander a
      | axis a == orientation = (layouts a ++)
      | null $ layouts a = id
      | otherwise = (a:)

update :: (Monad m, Rectangular a) =>
  (a -> m (Maybe a)) -> Layout a -> m (Maybe (Layout a))
update edit (Simple a) = fmap Simple <$> edit a
update edit (Array {axis, layouts}) = maybeArray <$> skim (update edit) layouts
  where
    maybeArray l = if null l then Nothing else Just $ array axis l

mergeInto :: Rectangular a => Axis -> [Layout a] -> [Layout a] -> Maybe (Layout a)
mergeInto orientation destination source
  | length destination < length source = mergeInto orientation source destination
  | otherwise = array (other orientation) <$> merge [] destination source
    where
      merge _ l [] = Just l
      merge _ [] l = Just l
      merge rearview dstColumns@(dstColumn:dstOthers) srcColumns@(srcColumn:srcOthers)
        | srcColumn |<| dstColumn =
          (srcColumn:) <$> merge rearview dstColumns srcOthers
        | connected orientation dstColumn srcColumn =
          let merged = append orientation dstColumn srcColumn in
          case (find (|<| dstColumn) rearview, find (dstColumn |<|) dstOthers) of
            (_, Just dstColumn2)
              | not (srcColumn |<| dstColumn2) -> Nothing
            (Just dstColumn2, _)
              | not (dstColumn2 |<| srcColumn) -> Nothing
            _ -> (merged:) <$> merge (dstColumn:rearview) dstOthers srcOthers
        | dstColumn /</ srcColumn =
          (dstColumn:) <$> merge (dstColumn:rearview) dstOthers srcColumns
        | otherwise = Nothing
      (|<|) = (if orientation == V then horizontally else vertically) [SLT]
      (/</) = (if orientation == V then horizontally else vertically) [SLT, LEQ]
