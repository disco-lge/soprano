{-# LANGUAGE NamedFieldPuns #-}
module Text.XML.Light.ALTO.RectElement (
      RectElement(..)
    , detectGeometry
    , getRectLines
    , getLines
    , identify
    , rectangle
    , setGeometry
    , setChildren
    , t
    , textBlock
    , topAligned
  ) where

import Text.Printf (printf)
import Text.XML.Light (Element(..), unode)
import Text.XML.Light.Extra (getAttr, setAttr, getTag)
import qualified Text.XML.Light.Extra as XML (setChildren)
import Text.XML.Light.ALTO.Geometry (
    Rectangle(..), Rectangular(..), get, hMax, vMax
  )
import Text.XML.Light.XPath (t, xPath)

data RectElement = RectElement {
      xml :: Element
    , geometry :: Rectangle
  } deriving Show

instance Rectangular RectElement where
  getRectangle = geometry

rectangle :: Element -> Either String RectElement
rectangle xml = do
  geometry <- Rectangle
    <$> attr "HPOS" <*> attr "VPOS" <*> attr "WIDTH" <*> attr "HEIGHT"
  return $ RectElement {xml, geometry}
  where
    attr s =
      maybe (Left $ "Missing attribute " ++ s) Right $ read <$> getAttr s xml

getLines :: RectElement -> [Element]
getLines = xPath [t"TextLine"] . elContent . xml

getRectLines :: RectElement -> [RectElement]
getRectLines e = [eElem | Right eElem <- rectangle <$> getLines e]

setGeometry :: Rectangle -> RectElement -> RectElement
setGeometry newGeometry@(Rectangle {hPos, vPos, width, height}) e =
  RectElement {
        xml = foldr setAttr (xml e) attributes
      , geometry = newGeometry
    }
  where
    attributes = fmap show <$>
      [("HPOS", hPos), ("VPOS", vPos), ("WIDTH", width), ("HEIGHT", height)]

detectGeometry :: Rectangular a => [a] -> Rectangle
detectGeometry [] = Rectangle 0 0 0 0
detectGeometry children =
  Rectangle {hPos = newHPos, vPos = newVPos, width, height}
  where
    newHPos = minimum $ get hPos <$> children
    width = (maximum $ get hMax <$> children) - newHPos
    newVPos = minimum $ get vPos <$> children
    height = (maximum $ get vMax <$> children) - newVPos

setChildren :: [RectElement] -> RectElement -> RectElement
setChildren children eElem = setGeometry (detectGeometry children) $ eElem {
    xml = XML.setChildren (xml <$> children) (xml eElem)
  }

textBlock :: [RectElement] -> RectElement
textBlock rectLines = setChildren rectLines $ RectElement {
      xml = unode "TextBlock" ()
    , geometry = detectGeometry ([] :: [RectElement])
  }

topAligned :: RectElement -> RectElement -> Bool
topAligned printSpace e = dV < ceiling (fromIntegral lineHeight / 2 :: Double)
  where
    dV = get vPos e - get vPos printSpace
    lineHeight =
      case getAttr "HEIGHT" <$> getLines e of
        ((Just height):otherLines)
          | not $ null otherLines -> read height
        _ -> get height e

identify :: RectElement -> String
identify (RectElement {xml}) = printf "%s%s" (getTag xml) altoID
  where
    altoID = maybe "" (' ':) $ getAttr "ID" xml
