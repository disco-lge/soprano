module Text.XML.Light.ALTO.Retrieve (
    blocks
  ) where

import Control.Monad.State (StateT, evalStateT, gets)
import Log (MonadLogger(..), LogLevel(..))
import Prelude hiding (log)
import Text.XML.Light.ALTO.Block.Caption as Caption (possibly)
import Text.XML.Light.ALTO.Block.Header as Header (possibly)
import Text.XML.Light.ALTO.Block.Pre (report)
import Text.XML.Light.ALTO.Block.Sixteenmo as Sixteenmo (
    check, numbering, textual
  )
import Text.XML.Light.ALTO.Geometry (get, height, leftOf, vPos, vMax)
import Text.XML.Light.ALTO.RectElement (
    RectElement(..), getRectLines, setChildren, textBlock
  )
import Utils ((.|.), scrollTo, splitState)

blocks :: MonadLogger m => [RectElement] -> m [RectElement]
blocks = evalStateT $ do
  topBlocks <- splitState topBlock
  sixteenmos <- getSixteenmos
  gets (\otherBlocks -> topBlocks ++ sixteenmos ++ otherBlocks)

topBlock :: MonadLogger m => RectElement -> m ([RectElement], RectElement)
topBlock rectElement =
  case getRectLines rectElement of
    (firstLine:otherLines)
      | Caption.possibly firstLine -> cutCaption firstLine otherLines
      | Header.possibly firstLine && not (null otherLines) ->
          report "peritext" firstLine >> split [firstLine] otherLines
    _ -> return ([], rectElement)
  where
    split floating bodyLines =
      return ([textBlock floating], setChildren bodyLines rectElement)
    gap (_, []) = False
    gap (a, (b:_)) = 2*(get vPos b - get vMax a) > min (get height a) (get height b)
    cutCaption firstLine otherLines =
      case scrollTo (gap .|. unAlignedLeft firstLine) (firstLine:otherLines) of
        (reversed, (lastLine:bodyLines)) ->
          let captionLines = reverse (lastLine:reversed) in
          report "caption" (head captionLines) >> split captionLines bodyLines
        (_, _) -> log Warning "Caption found but no gap" >> return ([], rectElement)
    unAlignedLeft _ (_, []) = False
    unAlignedLeft reference (_, (b:_)) = b `leftOf` reference

getSixteenmos :: MonadLogger m => StateT [RectElement] m [RectElement]
getSixteenmos = do
  textual16mos <- splitState Sixteenmo.textual
  folioNumber <- gets (any Sixteenmo.check) >>= getFolioNumber textual16mos
  return $ textual16mos ++ folioNumber
  where
    getFolioNumber [] False = return []
    getFolioNumber _ _ = splitState Sixteenmo.numbering
