{-# LANGUAGE NamedFieldPuns #-}
module Text.XML.Light.Extra (
      IndentConfig(..)
    , StickDirection(..)
    , attr
    , defaultIndentConfig
    , getAttr
    , getTag
    , indent
    , indentWith
    , raw
    , setAttr
    , setChildren
    , tag
  ) where

import Data.Map as Map ((!?), Map, empty)
import Data.Maybe (listToMaybe)
import Data.Set as Set (Set, member, empty)
import Text.XML.Light (
      Attr(..), CData(..), CDataKind(..), Content(..), Element(..), Node
    , QName(..), unode, unqual
  )

getTag :: Element -> String
getTag = qName . elName

attr :: String -> String -> Attr
attr key = Attr (unqual key)

tag :: Node t => String -> t -> Content
tag name = Elem . unode name

raw :: String -> Content
raw s = Text (CData {cdVerbatim = CDataText, cdData = s, cdLine = Nothing})

getAttr :: String -> Element -> Maybe String
getAttr attrName e =
  listToMaybe [attrVal a | a <- elAttribs e, qName (attrKey a) == attrName]

setAttr :: (String, String) -> Element -> Element
setAttr (attrName, attrVal) e =
  e {elAttribs = insertInto $ elAttribs e}
  where
    insertInto [] = [attr attrName attrVal]
    insertInto (attrib:attribs)
      | qName (attrKey attrib) == attrName = (attrib {attrVal}):attribs
      | otherwise = attrib:(insertInto attribs)

setChildren :: [Element] -> Element -> Element
setChildren children e = e {elContent = Elem <$> children}

data StickDirection = Previous | Next | Both

toNext :: StickDirection -> Bool
toNext Previous = False
toNext _ = True

toPrevious :: StickDirection -> Bool
toPrevious Next = False
toPrevious _ = True

data IndentConfig = IndentConfig {
      inlineElements :: Set QName
    , stickTo :: Map QName StickDirection
  }

defaultIndentConfig :: IndentConfig
defaultIndentConfig = IndentConfig Set.empty Map.empty

indentWith :: IndentConfig -> Int -> [Content] -> [Content]
indentWith _ _ [] = []
indentWith config level contents
  | level > 0 = (separate False $ indented <$> contents) ++ [separator $ level-1]
  | otherwise = indented <$> contents -- valid XML documents have only 1 root node, no need to separate
  where
    indented (Elem e)
      | not (Set.member (elName e) (inlineElements config)) = Elem $
          e {elContent = indentWith config (level+1) $ elContent e}
    indented c = c
    separator k = Text $ CData CDataRaw ('\n':replicate (2*k) ' ') Nothing
    doesStick p (Elem e) = maybe False p (stickTo config !? elName e)
    doesStick _ _ = True
    separate _ [] = []
    separate previousSticksNext (x:l) =
      let separatedTail = separate (doesStick toNext x) l in
      if previousSticksNext && doesStick toPrevious x
      then x:separatedTail
      else [separator level, x] ++ separatedTail

indent :: Int -> [Content] -> [Content]
indent = indentWith defaultIndentConfig
