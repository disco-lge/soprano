{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
module Text.XML.Light.TEI (
      Document(..)
    , dumpText
    , encode
    , indentConfig
  ) where

import Data.Char (isSpace)
import Data.Map as Map (fromList)
import Data.Set as Set (fromList)
import Prelude hiding (log)
import Text.XML.Light (
    Attr(..), Content, CData(..), Element(..), QName(..), unqual
  )
import qualified Text.XML.Light as XML (Content(..))
import Text.XML.Light.Extra (
    IndentConfig(..), StickDirection(..), attr, indentWith, raw, tag
  )

data Document = Document {
      docHead :: String
    , docId :: String
    , contents :: [Content]
    , prefix :: String
  }

indentConfig :: IndentConfig
indentConfig = IndentConfig {
      inlineElements = Set.fromList [head_, publisher, title]
    , stickTo = Map.fromList [
            (head_, Both)
          , (lb, Next)
        ]
  }
  where
    head_ = unqual "head"
    lb = unqual "lb"
    publisher = unqual "publisher"
    title = unqual "title"

encode :: Document -> [Content]
encode (Document {docHead, docId, contents, prefix}) =
  indentWith indentConfig 0 [
      tag"TEI" ([attr "xmlns" "http://www.tei-c.org/ns/1.0"], [
            tag"teiHeader" [
                tag"fileDesc" ([
                      tag"titleStmt" $
                        tag"title" (raw $ prefix ++ docHead)
                    , tag"publicationStmt" [
                            tag"publisher" (raw "CollEx-Persée DISCO-LGE")
                          , tag"date" [attr "when" "2021"]
                        ]
                    , tag"sourceDesc" $
                        tag"bibl" [
                              tag"title" (raw "La Grande Encyclopédie")
                            , tag"publisher" (raw "H. Lamirault")
                            , tag"date" [attr "from" "1885", attr "to" "1902"]
                          ]
                  ])
              ]
          , tag"text" $
              tag"body" $
                tag"div" ([Attr xmlId docId], contents)
        ])
    ]
  where
    xmlId = QName "id" Nothing (Just "xml")

dumpText :: Document -> [String]
dumpText = drop 1 . (foldr pickText [""]) . contents
  where
    pickText (XML.Text t) [] = [trimLeadingTrailingSpaces $ cdData t]
    pickText (XML.Text t) (t':l) =
      ((trimLeadingTrailingSpaces $ cdData t) ++ t'):l
    pickText (XML.Elem e) l
      | elName e == unqual "lb" = "":l
      | otherwise = foldr pickText l (elContent e)
    pickText _ l = l
    trimLeadingTrailingSpaces "" = ""
    trimLeadingTrailingSpaces (c:cs)
      | isSpace c = trimLeadingTrailingSpaces cs
      | otherwise = c:trimEnd (reverse cs)
    trimEnd (c:cs)
      | isSpace c = trimEnd cs
    trimEnd l = reverse l
