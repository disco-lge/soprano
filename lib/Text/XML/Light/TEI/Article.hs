{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
module Text.XML.Light.TEI.Article (
    segment
  ) where

import Context (Context(..), HasContext)
import Control.Monad.Reader (asks)
import Control.Monad.State (MonadState(state), StateT(..), evalStateT, gets, modify)
import Control.Monad.Trans (lift)
import Data.Char (isAlphaNum, isSpace, isUpper, toLower)
import Data.Map as Map (Map, empty, insert, lookup)
import Data.Bifunctor (first)
import Log (MonadLogger(..), LogLevel(..))
import Pipes (Pipe, await, yield)
import Prelude hiding (log)
import Text.XML.Light (Content)
import Text.XML.Light.ALTO.Block (Block(..))
import Text.XML.Light.ALTO.Block.Pre (BlockType(..))
import Text.XML.Light.ALTO.Dump (dumpLine)
import Text.XML.Light.ALTO.Geometry (Rectangle(..), get)
import Text.XML.Light.ALTO.RectElement (RectElement(..), getLines, getRectLines)
import Text.XML.Light.Extra (attr, raw, tag)
import Text.XML.Light.TEI (Document(..))
import Text.XML.Light.TEI.Article.SegmenterUtils (
    ArticleStart(..), getArticleStart, isIndented
  )
import Utils (zipUp)

data TmpTextBlock = TmpTextBlock {
      hReference :: Int
    , textLines :: [RectElement] 
  } deriving Show

data EncodingContext = EncodingContext {
      cached :: Maybe TmpTextBlock
    , headwords :: Map String Int
    , currentId :: Maybe (String, String)
    , stack :: [Content]
  }

type CachedBlock = Either TmpTextBlock Block
type StatefulPipe m = StateT EncodingContext (Pipe (Maybe Block) Document m)

freshContext :: EncodingContext
freshContext = EncodingContext {
      cached = Nothing
    , currentId = Nothing
    , headwords = Map.empty
    , stack = []
  }

cache :: Block -> CachedBlock
cache (Block {blockType = Text, blockElement}) = Left $ TmpTextBlock {
      hReference = get hPos $ geometry blockElement
    , textLines = getRectLines blockElement
  }
cache b = Right b

nextArticleStart :: (HasContext m, MonadLogger m) =>
  TmpTextBlock -> m ([String], Maybe (ArticleStart, TmpTextBlock))
nextArticleStart tmpTextBlock@(TmpTextBlock {hReference, textLines}) =
  first reverse <$> aux [] textLines
  where
    aux l [] = return (l, Nothing)
    aux l (nextLine:otherLines) = do
      indented <- isIndented nextLine hReference
      getArticleStart firstLine >>= emitIfFound indented
      where
        firstLine = dumpLine $ xml nextLine
        emitIfFound True (Just articleStart) = do
          log Info $ "Found article begining : " ++ headWord articleStart
          pure (l, Just (articleStart, tmpTextBlock {textLines = otherLines}))
        emitIfFound _ _ = aux (firstLine:l) otherLines

idOfHead :: String -> String
idOfHead "" = ""
idOfHead (c:cs)
  | isSpace c = idOfHead cs
  | isUpper c = toLower c : idOfHead cs
  | isAlphaNum c = c : idOfHead cs
  | otherwise = '-' : idOfHead cs

enter :: Monad m => String -> StateT EncodingContext m ()
enter newHead = do
  count <- gets (maybe 0 (+1) . Map.lookup prefix . headwords)
  let currentId = Just (newHead, prefix ++ '-':show count)
  let updated = Map.insert prefix count
  modify $ \ec -> ec {currentId, headwords = updated $ headwords ec}
  where
    prefix = idOfHead newHead

save :: Monad m => Maybe TmpTextBlock -> StateT EncodingContext m ()
save cached = modify $ \ec -> ec {cached}

nextCachedBlock :: Monad m => StatefulPipe m (Maybe CachedBlock)
nextCachedBlock =
  gets cached >>= maybe (fmap cache <$> lift await) (return . Just . Left)

push :: Monad m => [Content] -> StateT EncodingContext m ()
push contents = modify $ \ec -> ec {stack = zipUp contents (stack ec)}

takeContents :: Monad m => StateT EncodingContext m [Content]
takeContents = state $ \ec -> (reverse $ stack ec, ec {stack = []})

send :: (HasContext m) => [Content] -> StatefulPipe m ()
send contents = do
  prefix <- asks (maybe "" id . titlePrefix)
  gets currentId >>= checkAndWrap prefix
  where
    checkAndWrap _ Nothing = return ()
    checkAndWrap prefix (Just (docHead, docId)) =
      lift . yield $ Document {docHead, docId, contents, prefix}

findArticles :: (HasContext m, MonadLogger m) => StatefulPipe m ()
findArticles = nextCachedBlock >>= maybe (takeContents >>= send) searchBlock

searchBlock :: (HasContext m, MonadLogger m) =>
  CachedBlock -> StatefulPipe m ()
searchBlock (Right (Block {blockElement}))
  | null $ getLines blockElement = findArticles

searchBlock (Right (Block {blockType = Header, blockElement})) =
  push [xmlHeader blockElement] >> findArticles

searchBlock (Right (Block {blockType = Footer, blockElement})) =
  push [xmlFooter blockElement] >> findArticles

searchBlock (Right (PageBreak n)) =
  push [tag"pb" . attr "n" $ show n] >> findArticles

searchBlock (Right (Block {blockType = Caption, blockElement})) =
  push [tag"figure" $ tag"figDesc" (dumpTextBlock blockElement)] >> findArticles

searchBlock (Right (Block {blockType = Special, blockElement})) =
  push [tag"fw" (dumpTextBlock blockElement)] >> findArticles

searchBlock (Left tmpTextBlock) = do
  (articleLines, nextStart) <- nextArticleStart tmpTextBlock
  let textLines = articleLines >>= encodeLine
  case nextStart of
    Nothing -> push textLines >> save Nothing >> findArticles
    Just (ArticleStart {headWord, start}, rest) ->
        takeContents >>= send . (++ textLines)
      >> enter headWord
      >> push [tag"lb" (), tag"head" $ raw headWord, raw start]
      >> save (Just rest) >> findArticles

searchBlock e = fatal $ "Unexpected block looking for an article : " ++ show e

peritext :: String -> RectElement -> Content
peritext pType rectElement =
  tag"fw" (attr "type" pType, dumpTextBlock rectElement)

xmlHeader :: RectElement -> Content
xmlHeader = peritext "head"

xmlFooter :: RectElement -> Content
xmlFooter = peritext "foot"

dumpTextBlock :: RectElement -> String
dumpTextBlock = unlines . fmap dumpLine . getLines

encodeLine :: String -> [Content]
encodeLine l = [tag"lb" (), raw l]

segment :: (HasContext m, MonadLogger m) => Pipe (Maybe Block) Document m ()
segment = evalStateT findArticles freshContext
