{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
module Text.XML.Light.TEI.Article.SegmenterUtils (
      ArticleStart(..)
    , isIndented
    , getArticleStart
  ) where

import Context (Context(..), HasContext)
import Control.Monad.Reader (asks)
import Data.Char (isDigit, isPunctuation, isSpace)
import Data.Char.Extra (couldBe, upperCase)
import Data.Bifunctor (first)
import Log (MonadLogger(..))
import Text.Numeral.Roman (fromRoman)
import Text.XML.Light.ALTO.Geometry (Rectangle(..), get)
import Text.XML.Light.ALTO.RectElement (RectElement(..))
import Utils ((.|.))

data ArticleStart = ArticleStart {
      headWord :: String
    , start :: String
  }

isRomanNum :: String -> Bool
isRomanNum = maybe False (\_ -> True) . (fromRoman :: String -> Maybe Int)

getArticleStart :: MonadLogger m => String -> m (Maybe ArticleStart)
getArticleStart (c:cs)
  | looksUpperCase && notRoman && notDigitish =
      pure $ Just (ArticleStart {headWord, start})
  where
    (headWord, start) = first (c:) $ break ((`elem` ".,") .|. isSpace) cs
    looksUpperCase = all (couldBe upperCase .|. (`elem` "-'’")) headWord
    notRoman = not (isRomanNum headWord)
    notDigitish = not (all (isDigit .|. isPunctuation) headWord)
getArticleStart _ = pure Nothing

isIndented :: (HasContext m, MonadLogger m) =>
  RectElement -> Int -> m Bool
isIndented a hRef = asks ((dh >=) . (2*) . charWidth)
  where
    dh = get hPos a - hRef
