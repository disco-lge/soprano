module Text.XML.Light.XPath (
      Component(..)
    , childElements
    , matches
    , t
    , xPath
    , xPathCursor
  ) where

import Text.XML.Light (Content(..), Element(..))
import Text.XML.Light.Extra (getTag)
import Text.XML.Light.Cursor (Cursor(..), findRight, firstChild, parent)

data Component = Tag String | Predicate (Element -> Bool)

t :: String -> Component
t = Tag

matches :: Element -> Component -> Bool
matches e (Tag tagName) = getTag e == tagName
matches e (Predicate p) = p e

childrenLike :: Component -> [Content] -> [Element]
childrenLike component contents = [e | Elem e <- contents, e `matches` component]

childElements :: [Content] -> [Element]
childElements = xPath [Predicate $ \_ -> True]

xPath :: [Component] -> [Content] -> [Element]
xPath [] _ = []
xPath [component] contents = childrenLike component contents
xPath (component:components) contents =
  xPath components $ elContent =<< childrenLike component contents

xPathCursor :: [Component] -> Cursor -> Maybe Cursor
xPathCursor [] cursor = parent cursor
xPathCursor (component:components) cursor =
  findRight matching cursor >>= follow components
  where
    matching (Cur {current = Elem e}) = e `matches` component
    matching _ = False
    follow [] c = return c
    follow l c = firstChild c >>= xPathCursor l

