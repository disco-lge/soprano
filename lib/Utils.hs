{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BangPatterns #-}
module Utils (
      (.&.)
    , (.|.)
    , avg
    , ifM
    , mostly
    , popMin
    , push
    , scrollTo
    , skim
    , splitState
    , stats
    , stddev
    , takeAll
    , zipUp
  ) where

import Control.Applicative (liftA2)
import Control.Monad.State (MonadState(..), StateT(..), modify, state)
import Data.Foldable (foldrM)
import Data.List (foldl', partition)
import Data.Maybe (catMaybes)

stats :: Real a => [a] -> (Float, Float)
stats l =
  let a = total / fromIntegral count in
  (a, sqrt . avg $ (square . subtract a) <$> xs)
  where
    xs = realToFrac <$> l
    (count, total) = foldl' f (0 :: Int, 0) xs
    f (!k, !s) x = (k+1, s+x)
    square x = x * x

avg :: Real a => [a] -> Float
avg = fst . stats

stddev :: Real a => [a] -> Float
stddev = snd . stats

ifM :: Monad m => m Bool -> m a -> m a -> m a
ifM mB thenCase elseCase = mB >>= \b -> if b then thenCase else elseCase

mostly :: (a -> Bool) -> [a] -> Bool
mostly predicate l =
  let (witness, others) = partition predicate l in
  length witness > length others

popMin :: MonadState [a] m => (a -> Bool) -> (a -> a -> Bool) -> m (Maybe a)
popMin predicate lowerThan = state $ popMinAux (Nothing, [])
  where
    popMinAux m [] = m
    popMinAux (Nothing, l) (a:as)
      | predicate a = popMinAux (Just a, l) as
    popMinAux (Just m, l) (a:as)
      | predicate a && a `lowerThan` m = popMinAux (Just a, m:l) as
    popMinAux (found, l) (a:as) = popMinAux (found, a:l) as

push :: MonadState [a] m => a -> m ()
push = modify . (:)

splitState :: Monad m => (a -> m ([a], a)) -> StateT [a] m [a]
splitState splitter = StateT $ foldrM f ([], [])
  where
    f a (output, kept) = do
      (newOutput, newKept) <- splitter a
      return (newOutput ++ output, newKept:kept)

scrollTo :: ((a, [a]) -> Bool) -> [a] -> ([a], [a])
scrollTo predicate = scroller []
  where
    scroller passed [] = (passed, [])
    scroller passed next@(a:as)
      | predicate (a, as) = (passed, next)
      | otherwise = scroller (a:passed) as

skim :: Monad m => (a -> m (Maybe a)) -> [a] -> m [a]
skim skimmer = fmap catMaybes . mapM skimmer

takeAll :: MonadState [a] m => (a -> Bool) -> m [a]
takeAll = state . partition

zipUp :: [a] -> [a] -> [a]
zipUp context focused = foldl' (flip (:)) focused context

(.&.) :: Applicative f => f Bool -> f Bool -> f Bool
(.&.) = liftA2 (&&)

(.|.) :: Applicative f => f Bool -> f Bool -> f Bool
(.|.) = liftA2 (||)

