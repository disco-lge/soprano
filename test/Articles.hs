module Articles (
    testArticles
  ) where

import Control.Monad.IO.Class (MonadIO(..))
import Distribution.TestSuite (Progress(..), Result(..), Test, testGroup)
import Mock.Headwords (headwords)
import Mock.Resource (simpleTest)
import Text.XML.Light.TEI.Article.SegmenterUtils (getArticleStart)

testHeadword :: (String, Bool) -> Test
testHeadword (input, expected) = simpleTest (takeWhile (/= ' ') input) [] $ do
  actual <- maybe False (\_ -> True) <$> getArticleStart input
  Finished <$>
    if actual == expected
    then return Pass
    else do
      liftIO . putStrLn $
        "Expected «" ++ input ++ "» " ++ neg ++ "to be a headword"
      return (Fail $ "False " ++ kind)
  where
    (neg, kind) = if expected then ("", "negative") else ("not ", "positive")

testArticles :: Test
testArticles = testGroup "articles" [
    testGroup "headwords" $ testHeadword <$> headwords
  ]
