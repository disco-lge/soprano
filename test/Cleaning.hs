{-# LANGUAGE NamedFieldPuns #-}
module Cleaning (
    testCleaning
  ) where

import Control.Monad.IO.Class (liftIO)
import Data.Map as Map (Map, (!), fromList, keys)
import Distribution.TestSuite (Progress(..), Result(..), Test, testGroup)
import Mock.Resource (simpleTest)
import Mock.XML (leafElements)
import Text.XML.Light.ALTO.Clean (text)
import Text.XML.Light.ALTO.Dump (dumpString)
import Text.XML.Light.ALTO.RectElement (RectElement(..))
import Text.Printf (printf)

testFilterString :: String -> Test
testFilterString key = simpleTest testName [] $ do
  actual <- (fmap $ head . dumpString . xml) <$> text (leafElements ! key)
  Finished <$>
    if actual == filteredStrings ! key
    then return Pass
    else liftIO $ putStrLn (show actual) >> return (Fail "Wrong filtered output")
  where
    testName = printf "filter string %s" key

testFilterStrings :: Test
testFilterStrings = testGroup "filterString" $ testFilterString <$> inputs
  where
    inputs = Map.keys leafElements

filteredStrings :: Map String (Maybe String)
filteredStrings = Map.fromList [
      ("regular word", Just "Encyclopédie")
    , ("word with noise", Just "Encyclopédie")
    , ("word on denyList", Nothing)
  ]

testCleaning :: Test
testCleaning = testGroup "cleaning" [
    testFilterStrings
  ]
