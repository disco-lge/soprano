module Mock.Context (
    mockContext
  ) where

import Context (Context(..))
import Data.Set as Set (fromList)
import Text.XML.Light.ALTO.Block.Pre (BlockType(..))

mockContext :: Context
mockContext = Context {
      denyList = Set.fromList ["TO_DELETE"]
    , lineHeight = 50
    , charWidth = 20
    , noiseSet = Set.fromList "⬛■⏹⏺⯀⯁⯂⯃⯄⯅⯆⯇⯈●•®"
    , keep = Set.fromList [Header .. Special]
    , scoriaThreshold = 0.5
    , titlePrefix = Just "Test - "
  }
