module Mock.Headwords (
    headwords
  ) where

headwords :: [(String, Bool)]
headwords = [
      ("A (Ling.). Son vocal et première lettre de notre", True)
    , ("A (Paléogr.). C’est à l’alphabet phénicien, on le sait,", True)
    , ("L’A des différentes écritures dites nationales ne pré¬", False)
    , ("Il serait oiseux et du reste impraticable de vouloir", False)
    , ("A (Log.). Cette voyelle désigne les propositions uni¬", True)
    , ("H.M.", False)
    , ("A (Mus.). La lettre a est employée par les musiciens", True)
    , ("A (Numis.). Dans la numismatique grecque, la lettre A,", True)
    , ("AA. Ces deux lettres désignent l’atelier monétaire de", True)
    , ("AA. Nom de plusieurs cours d’eau de l’Europe occiden¬", True)
    , ("AA. Rivière de France, prend sa source aux Trois-", True)
    , ("AA. Rivière de Hollande, affluent de la Dommel. Elle", True)
    , ("AA. Nom de deux fleuves de la Russie. Le premier, celui de", True)
    , ("AA. Rivière de Suisse, affluent de droite de l’Aare. Elle", True)
    , ("AA (Saarner-AA). Rivière de Suisse, dans le canton", True)
    , ("AA (Méd.) (V. Abréviations).", True)
    , ("AA (Pierre van der), en latin Vanderanus, juriscon¬", True)
    , ("AA (Pierre van der), éditeur hollandais, mort vers", True)
    , ("AA (Christian-Charles-Henri van der), prédicateur et", True)
    , ("AA (Thierry van der), peintre hollandais, né en 1731", True)
    , ("AA (Cornelis van der), historien hollandais, né à Leyde", True)
    , ("AA (Pierre-Jean-Baptiste-Charles van der), fils de", True)
    , ("AAA (Numis.). Abréviation que l’on trouve fréquemment", True)
    , ("AACHEN (V. Aix-la-Chapelle).", True)
    , ("AACHÉNIEN (Géol.) (d’Aachen, Aix-la-Chapelle, par", True)
    , ("AAGAARD (Niels ou Nicolas), né en 4612, mort en", True)
    , ("AAGAARD (Christian), frère cadet du précédent, poète", True)
    , ("AAGESEN (Svend), premier historien danois, vivait à", True)
    , ("AAH. La lune est exprimée en égyptien par un sub¬", True)
    , ("AA H-H OTEP, reine d’Egypte, était considérée comme", True)
    , ("Paul Pierret.", False)
    , ("AALBORG (Albiœ). Ville de Danemark, à 222 kil.", True)
    , ("AALCLIM (Bot.). Nom donné, dans l’Inde, à plusieurs", True)
    , ("AALEN. Petite ville du Wurtemberg sur la Rocher.", True)
    , ("AALÉNIEN (Géol.) (d’Aalen, en Wurtemberg). Employé", True)
    , ("AALST (Géog.) (V. Alost).", True)
    , ("AAM. Mesure de capacité pour les liquides en usage en", True)
    , ("AA N S (Bot.) (V. Badamier).", True)
    , ("AA R, AA RO U, est le nom des Champs-Elysées des", True)
    , ("AA RA U. Ville de Suisse, capitale du cant. d’Argovie,", True)
    , ("AARBURG. Petite ville de Suisse, cant. d’Argovie,", True)
    , ("AA RE ou AA R. Principale rivière de Suisse, 280 kil.", True)
    , ("AARHUS. Ville de Danemark, sur la côte E. du Jut-", True)
    , ("AAROE. Petite île du Petit-Belt, du district de", True)
    , ("AARON, plus exactement AH ARON, personnage de l’an¬", True)
    , ("AARON ou AHRON, prêtre d’Alexandrie, médecin et", True)
    , ("AARON ou AHRON BEN EL1E (de Nicomédie), né ou", True)
    , ("Bibl. : Fürst, Gesch. des Karàerthums, II, p. 261.—", False)
    , ("AARON ou AHRON HACCOHEN, de Lunel, rabbin pro¬", True)
    , ("Bibl. : Gross, dans Monatschrift de Graetz, t. XVIII", False)
    , ("AARON ou AHRON HALLÉVI (ben Josef ben Ben veniste),", True)
    , ("Bibl. : Graetz, Gesch. d. Juden, t. VII. — Hist. liltér.", False)
    , ("AARON ou AHRON HALLÉVI, rabbin espagnol qui a", True)
    , ("AARON ou AHRON BEN JOSEF, rabbin caraïte. I", True)
    , ("Bibl. : Fürst, Gesch. des Karàerthums, II, p. 238. —", False)
    , ("AARON (Pierre) ou ARON, didacticien musical, né", True)
    , ("Voici les titres de ses ouvrages : 1° I Tre libri dell’", False)
    , ("AARON Ier, prince de Moldavie, mort entre le 19 octobre", True)
    , ("Bibl. : Ureciii, Chronique de Moldavie, édit. Picot, p.", False)
    , ("AARON II, prince de Moldavie (1591-1595), mort en", True)
    , ("AARONOWICZ (Izaac), savant israélite, originaire de", True)
    , ("AARSENS (François, van) ou AERSSENS, né à La Haye", True)
    , ("AARZIEHLE. Localité située près de Berne, à 500", True)
    , ("AASKOW (Urban-Bruun), médecin danois, né à Brôn", True)
    , ("AAV0RA (V. Avoira).", True)
    , ("AAZAZ (Château fort) (V. Azaz).", True)
    , ("AB. Nom du onzième mois du calendrier juif correspon¬", True)
    , ("ABA. Sorte de manteau oriental, n est porté en Turquie", True)
    , ("ABA ou ABBA. Ile du Nil blanc, l’une des mieux cul¬", True)
    , ("ABA, roi ou usurpateur de Hongrie, le représentant de", True)
    , ("A. Trasbot.", True)
    , ("Bibl. : G. F. Montucla, Histoire des mathématiques ;", False)
    , ("ABACO, architecte italien du xvi siècle (V. Labacco).", True)
    , ("ABACOCR1NUS. Genre d’Echinodcrmes fossiles créé", True)
    , ("ABACŒNUIVI. Ville de Sicile mentionnée par Diodore,", True)
    , ("ABACOPTERIS (Bot.). Ce nom a été donné par le bota¬", True)
    , ("ABACOT. Double couronne que portaient autrefois les", True)
    , ("AB ACT1S (V. Actuaire).", True)
    , ("ABACUC, prophète hébreu (V. Habacuc).", True)
    , ("ABACUS. L’abacus ou abaque était un instrument en", True)
    , ("Si les Grecs avaient eu l’idée de réformer leur système", False)
    , ("A. Trasbot.", False)
    , ("Biijl. : G. Libri, Histoire des sciences mathématiques", False)
    , ("AB AD (Rois maures de Séville) (V. Abbad).", True)
    , ("ABADDON ou APOLYON le Destructeur. « Elles", True)
    , ("ABADÈS (V. Ababdeh).", True)
    , ("ABADIE (Louis), compositeur de romances, mort en", True)
    , ("ABADIE (Paul), architecte, né à Bordeaux le 22 juil¬", True)
    , ("ABADIE (Paul), fils du précédent, architecte, né à Pans", True)
    , ("ABAD10TES ou ABDIOTES. Peuplade issue des Sar-", True)
    , ("ABAD1R ou ABADDIR. L’un des noms que portaient", True)
    , ("ABADZAS. Autre forme du mot Abazes (V. ce mot).", True)
    , ("ABÆ. Ville de Phocide, siège d’un très ancien oracle", True)
    , ("ABAFFY (Prince de Transylvanie) (V. Apaffy).", True)
    , ("ABAI (Bot.) (V. Chimonanthus).", True)
    , ("ABAILARD (Pierre). I. Histoire. — (Othon de Frisin-", True)
    , ("U. Philosophie. — C’est à Victor Cousin que nous", False)
    , ("Bibl. : Hist. littér. de la France, t. XII ( 1763 ),", False)
    , ("ABA1RUCU (Bot.) (V. Cynometra).", True)
    , ("ABAISSE. Terme de pâtisserie servant à désigner .un .", True)
    , ("ABAISSÉ (V. Blason).", True)
    , ("ABAISSEMENT. I. Mathématiques. — 1° Algèbre. En", True)
    , ("IL Théologie. — Un des éléments du mystère de l’In*", False)
    , ("Bibl. : Serret (J.-A.), Cours d’algèbre supérieure;", False)
    , ("ABAÏTÉ. Rivière du Brésil, affluent de gauche du Saô", True)
    , ("ABAJOUES. Poches membraneuses que certains genres", True)
    , ("ABAKAN. Rivière de Sibérie, affluent de la rive gauche", True)
    , ("ABAKUR. Nom de l’un des chevaux de Sunna (V. My¬", True)
    , ("ABALE (Malus). Ile de l’océan du Nord (Baltique), où", True)
    , ("ABALLO. Ville des Éduens (V. Avallon).", True)
    , ("AB A MA (Bot.). Dénomination générique proposée par", True)
    , ("ABAN (V. Calendrier Perse),", True)
    , ("ABANÇAY. Ville du Pérou, sur la rivière du même", True)
    , ("A3 AN CO U RT (François-Jean Willemain d’), homme de", True)
    , ("ABA'NCOURT (Charles-Xavier-Joseph de Franqueville", True)
    , ("ABANDON. I. Droit.— Cemotdésigne soit l’action de dé¬", True)
    , ("ABANDON DE CRÉDITS (Finances publiques). L’aban¬", True)
    , ("ABANDON DE PRIMES (Y. Primes).", True)
    , ("ABANDON DE SON POSTE (Justice militaire). Tout", True)
    , ("ABANDONNATES! R, ABAND0NNATA1RE. On appelle", True)
    , ("ABANDONNEMENT. I. Droit civil. — Ce mot est un", True)
    , ("IL Droit ecclésiastique. — En théorie, les seules peines", False)
    , ("ABANNATION. Ancien terme de jurisprudence qui", True)
    , ("ABANO, ou Abano bagni. Bourg d’Italie de la pro¬", True)
    , ("ABANO (Pierre d’), médecin et philosophe, né en", True)
    , ("ABANTES. Peuplade d’origine douteuse que l’on ren¬", True)
    , ("ABANTIADE. Nom générique signifiant descendant", True)
    , ("ABANTIDAS, fils de Paseas, tyran de Sicyone de 264 à", True)
    , ("ABAQUE. I. Antiquité.— Dans l’antiquité on donnait", True)
    , ("IL Architecture. — Tablette (figurée en A-B dans", False)
    , ("Bibl. : Guillaume, art. Abacus, VII, dans Daremberg", False)
    , ("ABARBANEL (V. Abravanel).", True)
    , ("ABARCA (Pierre), jésuite espagnol, né à Jaca en 1619,", True)
    , ("ABARCA (donJoaquin), évêque de Léon,né dansl’Aragon", True)
    , ("ABARCA DE BOLEA Y PORTUGAL (don Jérôme de),", True)
    , ("xvie siècle. Il entreprit d’écrire une Histoire du royaume", False)
    , ("ABAREMO-TEMO (Bot.). Nom sous lequel Pison", True)
    , ("(Brasil. 77) a décrit un arbre de la famille des Légu¬", False)
    , ("lobium).", False)
    , ("ABARES. Nom de deux peuples distincts, habitant, l’un", True)
    , ("ABARIM. Chaîne de montagnes de la Palestine au nord-", True)
    , ("ABARIS. Personnage plus qu’à demi légendaire, dont", True)
    , ("ABAS. Nom de plusieurs héros grecs ou catalogués par", True)
    , ("ABAS. Poids dont on se sert en Perse pour peser les", True)
    , ("(V. Abassi).", False)
    , ("ABASCAL (don José Fernando), général espagnol et", True)
    , ("il dut conquérir tous ses grades et les mériter par ses", False)
    , ("vit contre la République française. En 1796, Charles VI", False)
    , ("Nouvelle-Galice, puis la présidence de la cour royale de", False)
    , ("Il refusa de reconnaître le roi que Napoléon avait", False)
    , ("ABASCANTUS. Nom qui signifie « exempt de maléfice »,", True)
    , ("ABASECHS ou ABADZÈKHS. Peuplade de la race cir-", True)
    , ("ABASQUES ou ABASGES. Peuplade de la Colchide,", True)
    , ("ABASSI.Nom d’une monnaie de Perse, valant à peu", True)
    , ("ABATAGE. I. Sylviculture. — L’abatage est une opé¬", True)
    , ("IL Terme de boucherie. — Pour les animaux dont la", False)
    , ("111. Police sanitaire. — L’abatage est une mesure", False)
    , ("ABAT-CHAUVÉE ou ABAC-CHAUVÉE. Sorte de laine", True)
    , ("ABATELLEMENT. Suivant Savary, ce mot est employé", True)
    , ("Bibl. : Dictionnaire du commerce, t. I, p. 548.", False)
    , ("ABAT-FOIN. C’est l’ouverture pratiquée dans un gre¬", True)
    , ("ABATHMODON. Genre de Mammifères-Carnivores fossiles", True)
    , ("ABATIS. I. Cuisine. — On nomme abatis d’une vo¬", True)
    , ("ABAT-JOUR. Panneau en menuiserie placé devant", True)
    , ("ABATON. On donne ce nom à la partie (figurée en", True)
    , ("G. N.", False)
    , ("ABATOS. Ile du Nil, entourée de rochers, qui se trouvait", True)
    , ("ABATOS. C’est le nom que portait l’un des chevaux", True)
    , ("ABAT-SON. Petites lames, généralement de bois, pla¬", True)
    , ("Ils sont la plupart du temps recou¬", False)
    , ("ABATTEE ou ABATÉE (Marine). Ship's sheer; falling", True)
    , ("ABATTEMENT (Droit). L’abattement était autrefois en", True)
    , ("ABATTEMENT D’HONNEUR. Punition infligée au", True)
    , ("ABATTOIR. Etablissement communal affecté à l’aba¬", True)
    , ("ABATUS.Genre d’Echinodermes vivants et fossiles, créé", True)
    , ("J appartements. Les abat-", False)
    , ("ABAT-VOIX. Sorte de dais de bois ou de pierre placé", True)
    , ("ABAUJ-TORNA. L’un des comitats du nord de la Hon¬", True)
    , ("ABAUZIT (Firmin), savant français, né àUzèsle 11", True)
    , ("Bibl. : France protestante. — Le Protestant de Genève,", False)
    , ("ABAX (Abax Bonel.). Genre d’Tnsectes-CoIéoptères, du", True)
    , ("biaux, dont le deuxième article", False)
    , ("ABAYANCE ^Anc. jurisp.). Expression dont on se ser¬", True)
    , ("Bibl.: Houard, Anciennes Lois des Français ; Rouen,", False)
    , ("ABAZABS. Tribu bédouine qui habite les bords du Nil,", True)
    , ("ABAZES. Groupe de peuplades qui, avant 1861-66,", True)
    , ("ABBACOM1TAT. Mot qui désignait la qualité, l’état", True)
    , ("ABBAC0M1TE (V. Abbaye).", True)
    , ("ABBÂD Ier. Aboul-Qàsim-Mohammed ibn Ismaïl ibn", True)
    , ("ABBÂD II, surnommé al Motadhid, fils du précédent,", True)
    , ("ABBÂD III, surnommé al Motamid, né en 1040, mort", True)
    , ("ABBADIA (Luigia), cantatrice dramatique italienne,", True)
    , ("A6BADIE (Jacques), théologien protestant, ne a Nay", True)
    , ("Bibl. : France protestante.", False)
    , ("ABBADIE (Antoine Thomson d’), voyageur français né", True)
    , ("ABBADIE (Arnaud Michel d’), frère du précédent, né à", True)
    , ("ABBAD1TE ou ABBADIDE. Nom d’une famille arabe,", True)
    , ("Bibl. : R. Dozy, Histoire des Musulmans d’Espagne;", False)
    , ("AB B AD O N (Y. Abaddon).", True)
    , ("AB B A MARI, rabbin français, né àLunel au xive siècle.", True)
    , ("Bibl. : Hist. littér. de la France ; Paris, 1877 (tirage à", False)
    , ("Revue des études juives ; Paris, 1882, tome IV (Art. de", False)
    , ("ABBA RETZ. Corn, du dép. de la Loire-Inférieure, arr.", True)
    , ("AB BAS, oncle de Mohammed, né en 566, mort en 652.", True)
    , ("Il eut pour père Abd-al-Mouttalib et pour aïeul Hàchim et", False)
    , ("Tâlib, père d’Alî. Il était chargé de distribuer l’eau du puits", False)
    , ("ABBÂS Ier (le Grand), schâh de Perse, de la dynastie", True)
    , ("ABBÂS II, arrière-petit-fils d’Abbâs Ier, né en 1632", True)
    , ("ABBÂS III , dernier souverain de la dynastie des Séfé¬", True)
    , ("J. Preux.", False)
    , ("AB BAS-MI LES (Y. Abbaye).", True)
    , ("ABBÂS-M1RZA, héritier présomptif de Perse, né en", True)
    , ("ABBÂS-PACHA, vice-roi d’Egypte, né à Djeddah en", True)
    , ("ABBASIDES. Cette dynastie, la plus célèbre et la plus", True)
    , ("ABBATE (Niccolô dell’), peintre italien, né à Modène,", True)
    , ("Paul Mantz.", False)
    , ("Bibl. : Vedriani, Raccolta de'pittori modenesi; Modène,", False)
    , ("ABBATE ou ABBAT1 (Baldo-Angelo d’), Abbatius de", True)
    , ("ABBATIN1 (Guido Ubaldo), peintre italien, néà Città di", True)
    , ("A. Michel.", False)
    , ("Bibl. : Passeri, Vite dé pittori, scultori e architetti ;", False)
    , ("ABBATIS (Ab-ba-ti). Hérétiques vaudois de la fin du", True)
    , ("ABBATIS VILLA (V. Abbeville).", True)
    , ("ABBATOUNAS. Tribu cafre, qui habite la région située", True)
    , ("ABBATUCCI (Jacques-Pierre), général français, né en", True)
    , ("ABBATUCCI (Charles), général français, le plus cé¬", True)
    , ("ABBATUCCI (Jacques-Pierre-Charles), neveu du pré¬", True)
    , ("AB B AT U CCI (Séverin), troisième fils du précédent, homme", True)
    , ("ABBAVILLA (V. Abbeville).", True)
    , ("ABBAYE. I. Organisation écclésiastique. — Couvent", True)
    , ("E.-H. VoLLET.", False)
    , ("Plusieurs années déjà avant la Révolution, un certain", False)
    , ("Voici la liste des abbayes qui, par leur bonne conser¬", False)
    , ("ABBAYE (prison de P). Primitivement prison abba¬", True)
    , ("Bibl. '.Mémoires sur les journées de Septembre 1102,", False)
    , ("ABBAYE DE SAINTE-ESPÉRANCE ou de Sainte-", True)
    , ("ABBAYE-AU-BOIS. Petit village de France, hameau", True)
    , ("Bibl. : Lebeuf, Histoire du diocèse de Paris, t. VIII,", False)
    , ("ABBAYE-AU-BOIS. Nom que prit le couvent des", True)
    , ("ABBAYES et CHAPITRES NOBLES (V. Chapitres,)", True)
    , ("ABBAZiA. Petite localité d’Istrie à une heure et demie", True)
    , ("Bibl. : M. Schneider, Abbazia, Bayer, aerztliclie ln-", False)
    , ("ABBÉ, Abba, en syriaque : père. Les docteurs juifs", True)
    , ("E.-H. V.", False)
    , ("ABBE DE COUR (Petit-Abbé, Petit-Collet). L’abbé", True)
    , ("E.-H. Y.", False)
    , ("ABBÉ DES CORNARDS (V. Cornards).", True)
    , ("ABBÉ DES FOUS. Dans plusieurs chapitres, on don¬", True)
    , ("ABBE DU PEUPLE. Nom donné pendant quelque", True)
    , ("ABBÉ (Louis-Jean-Nicolas baron), général français, né", True)
    , ("ABBECOURT (Alba ou Alborumcuria). Hameau de la", True)
    , ("ABBEOKOUTA. Ville d’Afrique (Guinée septentrionale).", True)
    , ("ABBES (Aït). Tribu kabyle fort importante au siècle", True)
    , ("ABBESSE (V. Abbaye).", True)
    , ("ABBEVILLE (Abbatisvilla). Ville de France (Somme),", True)
    , ("Bibl. : F.-C. Louandre , Histoire d’Abbeville et du", False)
    , ("ABBEVILLE (Traité d’). On a donné ce nom à tort au", True)
    , ("Bibl : C. Bémont, Du nom de traité d'Abbeville, dans", False)
    , ("ABBEY (Jean), facteur d’orgues, né en 1785 à Wilton", True)
    , ("ABBIATE GRASSO. Ville d’Italie de la province et", True)
    , ("ABB1TIB1. Lacs et rivière de la Puissance (Dominion)", True)
    , ("Bibl. : H. M. Robinson, The great far land or siletches", False)
    , ("ABBON , moine de Saint-Germain des Prés et historien,", True)
    , ("ABBON {Abbo Floriacensis), abbé de Fleury-sur-Loire,", True)
    , ("B toi,. : Aimoin, Vita S. Abbonis abbatis Floriacensis,", False)
    , ("ABBOT (Robert), frère de Georges Abbot, né à Guild¬", True)
    , ("ABBOT (Georges), archevêque de Cantorbéry, né à", True)
    , ("Bibl.: Wood, Athenæ Oxonienses. — W. Russell, Life", False)
    , ("ABBOT (baron de Colchester, lord Charles), né à Abing-", True)
    , ("ABBOTSFORD. Ville d’Écosse, résidence de Walter", True)
    , ("ABBOTT (Jacob), écrivain américain,né le 14novembre", True)
    , ("Bibl. : John Nichol, American Literalure, and lristo-", False)
    , ("ABBOTT (John-Stevens-Cabot), historien américain,", True)
    , ("Bibl. : Congregational Quarterly; Boston, t. XX. — The", False)
    , ("ABBOTT (Henry), ingénieur américain, né à Beverly", True)
    , ("ABBOTT (Francis-Lemuel), peintre de portraits anglais,", True)
    , ("ABBOTT, acteur anglais, né à Chelsea en 1789.", True)
    , ("Bibl. : Chacun, Biographie clés acteurs anglais venus", False)
    , ("ABBRACCIAVACCA (Meo), poète italien du xine siècle,", True)
    , ("Bibl. : Nannucci, Manualc delta letteralura del primo", False)
    , ("AB BT (Thomas), moraliste allemand, né le 25 novembre", True)
    , ("A. Bossert.", False)
    , ("Bibl. : Herder, Ueber Thomas Abbls 'Schrif ten(Herders", False)
    , ("ABC (V. Aldhabet).", True)
    , ("ABCÈS (Chir.). Cavité accidentelle ou pathologique", True)
    , ("Quelle que soit leur cause, les abcès chauds ont pour signes", False)
  ]
