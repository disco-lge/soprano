{-# LANGUAGE NamedFieldPuns #-}
module Mock.Layout (
      allTheSame
    , array
    , column
    , row
    , sameStructure
  ) where

import Data.Foldable (toList)
import Text.XML.Light.ALTO.Geometry (Axis(..), Rectangle)
import Text.XML.Light.ALTO.Layout (Layout(..))
import Text.XML.Light.ALTO.RectElement (detectGeometry)

array :: Axis -> [Layout a] -> Layout a
array axis layouts = Array {axis, layouts, geometry = detectGeometry ([] :: [Layout Rectangle])}

column :: [a] -> Layout a
column = array V . fmap Simple

row :: [a] -> Layout a
row = array H . fmap Simple

sameStructure :: Eq a => Layout a -> Layout a -> Bool
sameStructure (Simple a) (Simple b) = a == b
sameStructure a@(Array {}) b@(Array {})
  | axis a == axis b = allTheSame sameStructure (layouts a) (layouts b)
sameStructure _ _ = False

allTheSame :: Foldable f => (a -> a -> Bool) -> f a -> f a -> Bool
allTheSame p a b =
  length a == length b && all id (zipWith p (toList a) (toList b))
