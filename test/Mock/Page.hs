module Mock.Page (
      Page
    , TextBlock
    , pagesGeometry
  ) where

import Data.Map as Map (Map, fromList)
import Text.XML.Light.ALTO.Geometry (Rectangle(..))

type Page = (Int, Int)
type TextBlock = (Int, Rectangle)

pagesGeometry :: Map Page [TextBlock]
pagesGeometry = Map.fromList [
      ((1, 37), zip [0..] [
            Rectangle {hPos = 2546, vPos = 493, width = 28, height = 34}
          , Rectangle {hPos = 1389, vPos = 502, width = 162, height = 40}
          , Rectangle {hPos = 382, vPos = 592, width = 1055, height = 295}
          , Rectangle {hPos = 1504, vPos = 573, width = 1073, height = 298}
          , Rectangle {hPos = 391, vPos = 2029, width = 1060, height = 285}
          , Rectangle {hPos = 1522, vPos = 2018, width = 1069, height = 287}
          , Rectangle {hPos = 394, vPos = 3388, width = 1062, height = 667}
          , Rectangle {hPos = 1528, vPos = 3393, width = 1068, height = 667}
        ])
    , ((1, 39), zip [0..] [
            Rectangle {hPos = 1413, vPos = 484, width = 178, height = 37}
          , Rectangle {hPos = 2453, vPos = 478, width = 154, height = 35}
          , Rectangle {hPos = 416, vPos = 557, width = 2192, height = 107}
          , Rectangle {hPos = 403, vPos = 2410, width = 1065, height = 1089}
          , Rectangle {hPos = 412, vPos = 3545, width = 186, height = 178}
          , Rectangle {hPos = 661, vPos = 3685, width = 60, height = 46}
          , Rectangle {hPos = 878, vPos = 3509, width = 601, height = 225}
          , Rectangle {hPos = 1533, vPos = 2413, width = 1074, height = 1616}
        ])
    , ((1, 58), zip [0..] [
            Rectangle {hPos = 727, vPos = 511, width = 533, height = 40}
          , Rectangle {hPos = 1736, vPos = 514, width = 186, height = 36}
          , Rectangle {hPos = 726, vPos = 595, width = 1068, height = 1002}
          , Rectangle {hPos = 731, vPos = 1651, width = 1044, height = 397}
          , Rectangle {hPos = 726, vPos = 2063, width = 1064, height = 379}
          , Rectangle {hPos = 727, vPos = 2480, width = 1054, height = 444}
          , Rectangle {hPos = 724, vPos = 3010, width = 1069, height = 1040}
          , Rectangle {hPos = 1864, vPos = 597, width = 1060, height = 195}
          , Rectangle {hPos = 2048, vPos = 798, width = 698, height = 867}
          , Rectangle {hPos = 1861, vPos = 1679, width = 1064, height = 616}
          , Rectangle {hPos = 2014, vPos = 2312, width = 758, height = 1340}
          , Rectangle {hPos = 1864, vPos = 3669, width = 1062, height = 390}
        ])
    , ((1, 60), zip [0..] [
            Rectangle {hPos = 727, vPos = 506, width = 201, height = 38}
          , Rectangle {hPos = 725, vPos = 586, width = 1068, height = 279}
          , Rectangle {hPos = 917, vPos = 913, width = 688, height = 1112}
          , Rectangle {hPos = 727, vPos = 2476, width = 1071, height = 422}
          , Rectangle {hPos = 729, vPos = 2904, width = 1070, height = 279}
          , Rectangle {hPos = 729, vPos = 3188, width = 1075, height = 566}
          , Rectangle {hPos = 730, vPos = 3758, width = 1075, height = 291}
          , Rectangle {hPos = 1862, vPos = 582, width = 1062, height = 191}
          , Rectangle {hPos = 1850, vPos = 795, width = 1078, height = 1863}
          , Rectangle {hPos = 1869, vPos = 2942, width = 1073, height = 1098}
        ])
    , ((1, 72), zip [0..] [
            Rectangle {hPos = 679, vPos = 513, width = 162, height = 36}
          , Rectangle {hPos = 1690, vPos = 500, width = 186, height = 35}
          , Rectangle {hPos = 681, vPos = 586, width = 1078, height = 758}
          , Rectangle {hPos = 687, vPos = 3155, width = 1083, height = 906}
          , Rectangle {hPos = 1821, vPos = 571, width = 1079, height = 762}
          , Rectangle {hPos = 1832, vPos = 3141, width = 1088, height = 910}
          , Rectangle {hPos = 666, vPos = 1407, width = 2244, height = 1585}
        ])
    , ((1, 74), zip [0..] [
            Rectangle {hPos = 736, vPos = 533, width = 163, height = 38}
          , Rectangle {hPos = 1821, vPos = 520, width = 115, height = 36}
          , Rectangle {hPos = 734, vPos = 602, width = 1069, height = 201}
          , Rectangle {hPos = 1874, vPos = 594, width = 1068, height = 192}
          , Rectangle {hPos = 984, vPos = 825, width = 1736, height = 2242}
          , Rectangle {hPos = 782, vPos = 3889, width = 1063, height = 198}
          , Rectangle {hPos = 1915, vPos = 3874, width = 1063, height = 202}
        ])
    , ((1, 79), zip [0..] [
            Rectangle {hPos = 2297, vPos = 491, width = 225, height = 39}
          , Rectangle {hPos = 335, vPos = 586, width = 1057, height = 490}
          , Rectangle {hPos = 1457, vPos = 573, width = 1071, height = 482}
          , Rectangle {hPos = 512, vPos = 1120, width = 1852, height = 2399}
          , Rectangle {hPos = 355, vPos = 3595, width = 1058, height = 425}
          , Rectangle {hPos = 1480, vPos = 3589, width = 1066, height = 426}
        ])
    , ((1, 82), zip [0..] [
            Rectangle {hPos = 721, vPos = 511, width = 384, height = 40}
          , Rectangle {hPos = 1729, vPos = 512, width = 184, height = 36}
          , Rectangle {hPos = 720, vPos = 588, width = 1083, height = 1429}
          , Rectangle {hPos = 776, vPos = 2034, width = 995, height = 840}
          , Rectangle {hPos = 743, vPos = 2950, width = 1074, height = 1099}
          , Rectangle {hPos = 1854, vPos = 581, width = 1102, height = 3462}
        ])
    , ((1, 157), zip [0..] [
            Rectangle {hPos = 1335, vPos = 517, width = 138, height = 36}
          , Rectangle {hPos = 2029, vPos = 510, width = 458, height = 45}
          , Rectangle {hPos = 283, vPos = 602, width = 1057, height = 431}
          , Rectangle {hPos = 381, vPos = 1062, width = 853, height = 602}
          , Rectangle {hPos = 281, vPos = 1681, width = 1060, height = 2423}
          , Rectangle {hPos = 1411, vPos = 602, width = 1077, height = 287}
          , Rectangle {hPos = 1639, vPos = 916, width = 619, height = 36}
          , Rectangle {hPos = 1415, vPos = 952, width = 1074, height = 635}
          , Rectangle {hPos = 1892, vPos = 1628, width = 115, height = 37}
          , Rectangle {hPos = 1412, vPos = 1672, width = 1077, height = 1244}
          , Rectangle {hPos = 1428, vPos = 2925, width = 1006, height = 643}
          , Rectangle {hPos = 1405, vPos = 3620, width = 1083, height = 479}
        ])
    , ((1, 231), zip [0..] [
            Rectangle {hPos = 1293, vPos = 537, width = 139, height = 36}
          , Rectangle {hPos = 2293, vPos = 531, width = 207, height = 43}
          , Rectangle {hPos = 320, vPos = 621, width = 445, height = 427}
          , Rectangle {hPos = 859, vPos = 618, width = 524, height = 417}
          , Rectangle {hPos = 1420, vPos = 619, width = 524, height = 428}
          , Rectangle {hPos = 2019, vPos = 625, width = 445, height = 34}
          , Rectangle {hPos = 1979, vPos = 667, width = 520, height = 322}
          , Rectangle {hPos = 322, vPos = 1064, width = 1053, height = 429}
          , Rectangle {hPos = 1441, vPos = 1063, width = 1069, height = 419}
          , Rectangle {hPos = 348, vPos = 1503, width = 2137, height = 95}
          , Rectangle {hPos = 344, vPos = 1632, width = 2166, height = 2447}
        ])
    , ((1, 1248), zip [0..] [
            Rectangle {hPos = 727, vPos = 1220, width = 2109, height = 1653}
          , Rectangle {hPos = 1326, vPos = 2883, width = 932, height = 40}
          , Rectangle {hPos = 700, vPos = 2982, width = 1069, height = 920}
          , Rectangle {hPos = 1833, vPos = 2973, width = 1075, height = 912}
          , Rectangle {hPos = 1577, vPos = 4009, width = 472, height = 34}
          , Rectangle {hPos = 1830, vPos = 4207, width = 134, height = 31}
          , Rectangle {hPos = 695, vPos = 508, width = 429, height = 47}
          , Rectangle {hPos = 1649, vPos = 510, width = 240, height = 37}
          , Rectangle {hPos = 687, vPos = 596, width = 1060, height = 578}
          , Rectangle {hPos = 1816, vPos = 594, width = 1070, height = 572}
        ])
    , ((7, 106), zip [0..] [
            Rectangle {hPos = 717, vPos = 569, width = 214, height = 38}
          , Rectangle {hPos = 1716, vPos = 553, width = 116, height = 38}
          , Rectangle {hPos = 721, vPos = 639, width = 1081, height = 3502}
          , Rectangle {hPos = 1858, vPos = 625, width = 1082, height = 1492}
          , Rectangle {hPos = 1906, vPos = 2152, width = 1030, height = 1836}
          , Rectangle {hPos = 1870, vPos = 4034, width = 1075, height = 103}
          , Rectangle {hPos = 1167, vPos = 1039, width = 1337, height = 844}
          , Rectangle {hPos = 1229, vPos = 1914, width = 1204, height = 47}
        ])
  ]
