{-# LANGUAGE NamedFieldPuns #-}
module Mock.Resource (
      TestStack
    , loadMockData
    , simpleTest
  ) where

import Context (Context)
import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Reader (ReaderT, runReaderT)
import Distribution.TestSuite (
    Progress(..), Result(..), Test(..), TestInstance(..)
  )
import Log (LogLevel(..), LoggerT, handle, io)
import Mock.Context (mockContext)
import System.FilePath ((</>))

type TestStack = ReaderT Context (LoggerT IO)

mockDataDir :: FilePath
mockDataDir = "test/Mock/data"

loadMockData :: MonadIO m => String -> m String
loadMockData = liftIO . readFile . (mockDataDir </>)

simpleTest :: String -> [String] -> TestStack Progress -> Test
simpleTest name tags test =
  Test $ TestInstance {run, name, tags, options = [], setOption}
  where
    setOption _ _ = Left "No option available"
    run = handle (runReaderT test mockContext)
                 (io Log.Error)
                 (return . Finished . Fail)
