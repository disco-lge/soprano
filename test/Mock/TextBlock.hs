module Mock.TextBlock (
      load
    , loadAll
  ) where

import Control.Monad ((>=>))
import Control.Monad.IO.Class (MonadIO(..))
import Log (MonadLogger(..))
import qualified Mock.XML as XML (getTextBlocks, load)
import Text.XML.Light.ALTO.RectElement (RectElement, rectangle)

load :: (MonadLogger m, MonadIO m) => String -> m RectElement
load = loadAll >=> expectOne
  where
    expectOne [] = fatal "Expected a RectElement but none found"
    expectOne [x] = return x
    expectOne _ = fatal "Expected 1 RectElement but more were found"

loadAll :: (MonadLogger m, MonadIO m) => String -> m [RectElement]
loadAll x =
  XML.load x >>= either fatal return . (mapM rectangle . XML.getTextBlocks)
