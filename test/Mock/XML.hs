module Mock.XML (
      leafElements
    , getTextBlocks
    , load
  ) where

import Control.Monad.IO.Class (MonadIO(..))
import Data.Map as Map (Map, fromList)
import Mock.Resource (loadMockData)
import System.FilePath ((<.>))
import Text.XML.Light (Content, Element, parseXML, parseXMLDoc)
import Text.XML.Light.ALTO.RectElement (RectElement, rectangle)
import Text.XML.Light.XPath (t, xPath) 

load :: MonadIO m => String -> m [Content]
load = fmap parseXML . loadMockData . (<.> "xml")

getTextBlocks :: [Content] -> [Element]
getTextBlocks = xPath [t"TextBlock"]

leafElements :: Map String RectElement
leafElements = Map.fromList [(a, c) | (a, Just b) <- fmap parseXMLDoc <$> [
      ("regular word", "<String ID=\"P112_S00002\" HPOS=\"1510\" VPOS=\"514\" WIDTH=\"45\" HEIGHT=\"14\" STYLEREFS=\"StyleId-0\" CONTENT=\"Encyclopédie\" WC=\"0.1\" CC=\"9\" />")
    , ("word with noise", "<String ID=\"P112_S00002\" HPOS=\"1510\" VPOS=\"514\" WIDTH=\"45\" HEIGHT=\"14\" STYLEREFS=\"StyleId-0\" CONTENT=\"Encyclo■pédie\" WC=\"0.1\" CC=\"9\" />")
    , ("word on denyList", "<String ID=\"TO_DELETE\" HPOS=\"1510\" VPOS=\"514\" WIDTH=\"45\" HEIGHT=\"14\" STYLEREFS=\"StyleId-0\" CONTENT=\"Encyclopédie\" WC=\"0.1\" CC=\"9\" />")
  ], Right c <- rectangle <$> [b]]
