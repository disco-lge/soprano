module Peritext (
    testPeritexts
  ) where

import Control.Monad.IO.Class (MonadIO(..))
import Distribution.TestSuite (Progress(..), Result(..), Test, testGroup)
import Mock.Layout as Mock (allTheSame, array, column, row, sameStructure)
import Mock.Resource (simpleTest)
import Mock.TextBlock (loadAll)
import Text.XML.Light.ALTO.Block.Peritext (PageRows(..), separate)
import Text.XML.Light.ALTO.Block.Sixteenmo as Sixteenmo (rate)
import Text.XML.Light.ALTO.Geometry (Axis(..))
import Text.XML.Light.ALTO.Layout as Layout (Layout(..), arrange, array)
import Text.XML.Light.ALTO.RectElement (RectElement(..))
import Text.XML.Light.Extra (getAttr)

testRate :: (String, Float) -> Test
testRate (fileName, expected) = simpleTest fileName [] $ do
  actual <- Sixteenmo.rate . Layout.array H . fmap Simple <$> loadAll fileName
  Finished <$>
    if actual == expected
    then return Pass
    else
      liftIO $ putStrLn (show actual) >> return (Fail "Wrong rating")

rates :: [(String, Float)]
rates = [
    ("footer_T1p35", 4)
  ]

testSeparate :: (String, PageRows String) -> Test
testSeparate (fileName, expected) = simpleTest fileName [] $ do
  actual <- loadAll fileName >>= arrange >>= fmap keepIDs . separate . layouts
  Finished <$>
    if checkStructure actual expected
    then return Pass
    else
      liftIO $ putStrLn (show actual) >> return (Fail "Did not separate properly")
  where
    keepIDs = fmap (maybe "<no ID>" id . getAttr "ID" . xml)
    checkStructure a b =
        allTheSame sameStructure (beforeRows a) (beforeRows b)
      && allTheSame sameStructure (headRow a) (headRow b)
      && allTheSame sameStructure (middleRows a) (middleRows b)
      && allTheSame sameStructure (footRow a) (footRow b)
      && allTheSame sameStructure (afterRows a) (afterRows b)

pages :: [(String, PageRows String)]
pages = [
      ("T1p35", PageRows {
          beforeRows = []
        , headRow = Just (Simple "P34_TB00001")
        , middleRows = [row ["P34_TB00002", "P34_TB00003"]]
        , footRow = Just (row ["P34_TB00004", "P34_TB00005"])
        , afterRows = []
      })
    , ("T1p36", PageRows {
          beforeRows = []
        , headRow = Nothing
        , middleRows = [Simple "P35_TB00001", Simple "P35_TB00002"]
        , footRow = Nothing
        , afterRows = []
      })
    , ("T1p51", PageRows {
          beforeRows = []
        , headRow = Just $ row ["P50_TB00001", "P50_TB00002"]
        , middleRows = [row ["P50_TB00003", "P50_TB00004"]]
        , footRow = Just $ row ["P50_TB00005", "P50_TB00006"]
        , afterRows = []
      })
    , ("T1p518", PageRows {
          beforeRows = []
        , headRow = Just $ row ["P517_TB00001", "P517_TB00002"]
        , middleRows = [Mock.array H [
                              Simple "P517_TB00003"
                            , column [
                                  "P517_TB00004", "P517_TB00005", "P517_TB00006"
                                ]
                          ]]
        , footRow = Nothing
        , afterRows = []
      })
  ]

testPeritexts :: Test
testPeritexts = testGroup "peritexts" [
      testGroup "rates" $ testRate <$> rates
    , testGroup "separate" $ testSeparate <$> pages
  ]
