module RectElement (
    testAllRetrieveFloating
  ) where

import Distribution.TestSuite (Progress(..), Result(..), Test, testGroup)
import Mock.TextBlock as TextBlock (loadAll)
import Mock.Resource (simpleTest)
import Text.Printf (printf)
import Text.XML.Light.ALTO.Retrieve as Retrieve (blocks)

testRetrieveFloating :: (String, Int) -> Test
testRetrieveFloating (fileName, expected) =
  simpleTest ("blocks retrieving - " ++ fileName) [] $ do
    TextBlock.loadAll fileName >>= tryRetrieveFloating
  where
    tryRetrieveFloating rectElements = do
      actual <- length <$> Retrieve.blocks rectElements
      return . Finished $
        if actual == expected
        then Pass
        else Fail $
          printf "Found %d instead of %d TextBlocks" actual expected

testAllRetrieveFloating :: Test
testAllRetrieveFloating =
  testGroup "blocks retrieving" $ testRetrieveFloating <$> files
  where
    files = [
          ("peritext_T1p77", 2)
        , ("peritext_T1p131", 4)
        , ("peritext_T1p845", 2)
        , ("caption_T1p834", 2)
        , ("peritext_only_T1p713", 2)
        , ("caption_T1p839", 4)
        , ("caption_T7p134", 2)
        , ("caption_T1p157", 2)
        , ("peritext_T1p112", 2)
        , ("peritext_T9p102_top", 2)
        , ("peritext_T9p102_bottom", 3)
      ]
