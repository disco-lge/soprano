module Tests (
    tests
  ) where

import Articles (testArticles)
import Cleaning (testCleaning)
import Distribution.TestSuite (Test)
import Ordering (testLayouts)
import Peritext (testPeritexts)
import RectElement (testAllRetrieveFloating)
import XML (testXPath)

tests :: IO [Test]
tests = return [
      testXPath
    , testCleaning
    , testAllRetrieveFloating
    , testLayouts
    , testPeritexts
    -- disabled for now, headword detection still needs to be improved
    --, testArticles
  ]
