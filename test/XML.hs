module XML (
    testXPath
  ) where

import Distribution.TestSuite (Progress(..), Result(..), Test, testGroup)
import Mock.Resource (simpleTest)
import Mock.XML as XML (getTextBlocks, load)

testXPath :: Test
testXPath = testGroup "xPath" $ openTextBlock <$> files
  where
    files = ["peritext_T1p77", "peritext_T1p845", "caption_T1p834"]
    openTextBlock fileName = simpleTest ("xPath - " ++ fileName) [] $ do
      input <- XML.load fileName
      return . Finished $ case XML.getTextBlocks input of
        [_] -> Pass
        l -> Fail $
          (if null l then "No" else "More than 1") ++ " TextBlock found in " ++ fileName
